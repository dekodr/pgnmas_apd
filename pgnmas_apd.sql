-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 17, 2019 at 02:54 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.0.32-4+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pgnmas_apd`
--
CREATE DATABASE IF NOT EXISTS `pgnmas_apd` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pgnmas_apd`;

-- --------------------------------------------------------

--
-- Table structure for table `ms_cart`
--

DROP TABLE IF EXISTS `ms_cart`;
CREATE TABLE `ms_cart` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `session` longtext,
  `entry_stamp` timestamp NULL DEFAULT NULL,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_company`
--

DROP TABLE IF EXISTS `ms_company`;
CREATE TABLE `ms_company` (
  `id` int(11) NOT NULL,
  `company_code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` text,
  `is_budget` tinyint(1) DEFAULT '0',
  `entry_stamp` timestamp NULL DEFAULT NULL,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_do`
--

DROP TABLE IF EXISTS `ms_do`;
CREATE TABLE `ms_do` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `id_vendor` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_po` int(11) NOT NULL,
  `do_no` varchar(40) NOT NULL,
  `remark` text NOT NULL,
  `notes` longtext,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_do_detail`
--

DROP TABLE IF EXISTS `ms_do_detail`;
CREATE TABLE `ms_do_detail` (
  `id` int(11) NOT NULL,
  `id_do` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_order_detail` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_item_company` int(11) NOT NULL,
  `id_price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_item`
--

DROP TABLE IF EXISTS `ms_item`;
CREATE TABLE `ms_item` (
  `id` int(11) NOT NULL,
  `id_category` int(11) DEFAULT '0',
  `id_unit` int(11) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `rd_code` varchar(6) DEFAULT NULL,
  `name` varchar(70) DEFAULT NULL,
  `description` text,
  `tags` text,
  `year` int(4) DEFAULT NULL,
  `entry_stamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_item_category`
--

DROP TABLE IF EXISTS `ms_item_category`;
CREATE TABLE `ms_item_category` (
  `id` int(11) NOT NULL,
  `id_group` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_eb` int(6) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  `entry_stamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_item_company`
--

DROP TABLE IF EXISTS `ms_item_company`;
CREATE TABLE `ms_item_company` (
  `id` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_item_images`
--

DROP TABLE IF EXISTS `ms_item_images`;
CREATE TABLE `ms_item_images` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `name` text NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_order`
--

DROP TABLE IF EXISTS `ms_order`;
CREATE TABLE `ms_order` (
  `id` int(11) NOT NULL,
  `rd_id` int(11) DEFAULT NULL,
  `id_company` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_group_master` int(11) DEFAULT NULL,
  `order_date` date NOT NULL,
  `order_status` int(11) NOT NULL DEFAULT '1' COMMENT '1-approved 2-rejected by admin',
  `approve_date` timestamp NULL DEFAULT NULL,
  `is_approve` int(11) NOT NULL DEFAULT '0' COMMENT '0 - abandon, 1 - approved, 2 - rejected.',
  `is_invoice` tinyint(1) NOT NULL DEFAULT '0',
  `total` bigint(20) DEFAULT NULL,
  `reason` text,
  `description` text,
  `note` longtext,
  `entry_stamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_order_detail`
--

DROP TABLE IF EXISTS `ms_order_detail`;
CREATE TABLE `ms_order_detail` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_item_company` int(11) NOT NULL,
  `id_price` int(11) NOT NULL,
  `is_handled` tinyint(1) NOT NULL,
  `quantity_received` bigint(20) DEFAULT NULL,
  `is_status` tinyint(4) NOT NULL COMMENT '1 - approved, 2 - po, 3 - do, 4 - delivered.',
  `is_finished` tinyint(4) NOT NULL,
  `quantity` int(11) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_po`
--

DROP TABLE IF EXISTS `ms_po`;
CREATE TABLE `ms_po` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_vendor` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `po_no` varchar(40) DEFAULT NULL,
  `po_date` date NOT NULL,
  `remark` text NOT NULL,
  `notes` longtext,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_po_detail`
--

DROP TABLE IF EXISTS `ms_po_detail`;
CREATE TABLE `ms_po_detail` (
  `id` int(11) NOT NULL,
  `id_po` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_order_detail` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_item_company` int(11) NOT NULL,
  `id_price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_unit`
--

DROP TABLE IF EXISTS `ms_unit`;
CREATE TABLE `ms_unit` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `rd_code` int(11) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_user`
--

DROP TABLE IF EXISTS `ms_user`;
CREATE TABLE `ms_user` (
  `id` int(11) NOT NULL,
  `id_role` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `entry_stamp` timestamp NULL DEFAULT NULL,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_user`
--

INSERT INTO `ms_user` (`id`, `id_role`, `id_company`, `name`, `email`, `entry_stamp`, `edit_stamp`, `del`) VALUES
(1, 1, 1, 'admin', 'admin@admin.com', '2019-01-16 17:00:00', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ms_user_role`
--

DROP TABLE IF EXISTS `ms_user_role`;
CREATE TABLE `ms_user_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(50) DEFAULT NULL,
  `entry_stamp` timestamp NULL DEFAULT NULL,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_vendor`
--

DROP TABLE IF EXISTS `ms_vendor`;
CREATE TABLE `ms_vendor` (
  `id` int(11) NOT NULL,
  `id_group` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_admin_pos` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_wishlist`
--

DROP TABLE IF EXISTS `ms_wishlist`;
CREATE TABLE `ms_wishlist` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_eb` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` longtext,
  `min_price` bigint(20) DEFAULT NULL,
  `max_price` bigint(11) DEFAULT NULL,
  `images` text,
  `is_approve` int(11) NOT NULL DEFAULT '0' COMMENT '0 - abandon, 1 - approved, 2 - rejected.',
  `reason` text,
  `entry_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_wishlist_images`
--

DROP TABLE IF EXISTS `ms_wishlist_images`;
CREATE TABLE `ms_wishlist_images` (
  `id` bigint(20) NOT NULL,
  `id_wishlist` bigint(20) NOT NULL,
  `name` text NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `del` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id`, `name`, `del`) VALUES
(1, 'Admin', 0),
(2, 'Vendor', 0),
(3, 'User', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_invoice`
--

DROP TABLE IF EXISTS `tr_invoice`;
CREATE TABLE `tr_invoice` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `invoice_date` date NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_invoice_detail`
--

DROP TABLE IF EXISTS `tr_invoice_detail`;
CREATE TABLE `tr_invoice_detail` (
  `id` int(11) NOT NULL,
  `id_invoice` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_area` int(11) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_order_status`
--

DROP TABLE IF EXISTS `tr_order_status`;
CREATE TABLE `tr_order_status` (
  `id` int(11) NOT NULL,
  `id_order_detail` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `status_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entry_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_password`
--

DROP TABLE IF EXISTS `tr_password`;
CREATE TABLE `tr_password` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `entry_stamp` timestamp NULL DEFAULT NULL,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_password`
--

INSERT INTO `tr_password` (`id`, `id_user`, `password`, `is_active`, `entry_stamp`, `edit_stamp`, `del`) VALUES
(1, 1, '123', 1, '2019-01-16 17:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_price`
--

DROP TABLE IF EXISTS `tr_price`;
CREATE TABLE `tr_price` (
  `id` int(11) NOT NULL,
  `id_item_company` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `value` bigint(20) NOT NULL,
  `entry_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `edit_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_username`
--

DROP TABLE IF EXISTS `tr_username`;
CREATE TABLE `tr_username` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `entry_stamp` timestamp NULL DEFAULT NULL,
  `edit_stamp` timestamp NULL DEFAULT NULL,
  `del` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_username`
--

INSERT INTO `tr_username` (`id`, `id_user`, `username`, `is_active`, `entry_stamp`, `edit_stamp`, `del`) VALUES
(1, 1, 'admin', 1, '2019-01-16 17:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_cart`
--
ALTER TABLE `ms_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_company`
--
ALTER TABLE `ms_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_do`
--
ALTER TABLE `ms_do`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_do_detail`
--
ALTER TABLE `ms_do_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_item`
--
ALTER TABLE `ms_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_item_category`
--
ALTER TABLE `ms_item_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_item_company`
--
ALTER TABLE `ms_item_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_item_images`
--
ALTER TABLE `ms_item_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_order`
--
ALTER TABLE `ms_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_order_detail`
--
ALTER TABLE `ms_order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_po`
--
ALTER TABLE `ms_po`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_po_detail`
--
ALTER TABLE `ms_po_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_unit`
--
ALTER TABLE `ms_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_user`
--
ALTER TABLE `ms_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_user_role`
--
ALTER TABLE `ms_user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_vendor`
--
ALTER TABLE `ms_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_wishlist`
--
ALTER TABLE `ms_wishlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_wishlist_images`
--
ALTER TABLE `ms_wishlist_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_invoice`
--
ALTER TABLE `tr_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_order_status`
--
ALTER TABLE `tr_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_password`
--
ALTER TABLE `tr_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_price`
--
ALTER TABLE `tr_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_username`
--
ALTER TABLE `tr_username`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_cart`
--
ALTER TABLE `ms_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_company`
--
ALTER TABLE `ms_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_do`
--
ALTER TABLE `ms_do`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_do_detail`
--
ALTER TABLE `ms_do_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_item`
--
ALTER TABLE `ms_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_item_category`
--
ALTER TABLE `ms_item_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_item_company`
--
ALTER TABLE `ms_item_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_item_images`
--
ALTER TABLE `ms_item_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_order`
--
ALTER TABLE `ms_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_order_detail`
--
ALTER TABLE `ms_order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_po`
--
ALTER TABLE `ms_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_po_detail`
--
ALTER TABLE `ms_po_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_unit`
--
ALTER TABLE `ms_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_user`
--
ALTER TABLE `ms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ms_user_role`
--
ALTER TABLE `ms_user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_vendor`
--
ALTER TABLE `ms_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_wishlist`
--
ALTER TABLE `ms_wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ms_wishlist_images`
--
ALTER TABLE `ms_wishlist_images`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tr_invoice`
--
ALTER TABLE `tr_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_order_status`
--
ALTER TABLE `tr_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_password`
--
ALTER TABLE `tr_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tr_price`
--
ALTER TABLE `tr_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tr_username`
--
ALTER TABLE `tr_username`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
