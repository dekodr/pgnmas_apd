<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_model{



	function __construct(){

		parent::__construct();

	}
	

	function cek_login(){

		/*===================== LEGEND =====================

			$sql_un 	=> sql syntax for username;
			$sql_pw 	=> sql syntax for password;
			$sql_us 	=> sql syntax for user;
			$sql_role 	=> sql syntax for role name;

		  ===================== ______ =====================*/

		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');


		/*check if username is exist*/
		$sql_un 	= "SELECT * FROM tr_username WHERE username = ? AND is_active = 1 ORDER BY id DESC";
		$sql_un 	= $this->db->query($sql_un, $username);
		$sql_un 	= $sql_un->row_array();

		$id_user	= $sql_un['id_user'];
		$ct_sql 	= '';

		/*if username exist*/
		if($sql_un){

			/*check if username match with current password*/
			$sql_pw = "SELECT * FROM tr_password WHERE id_user = ? AND password = ? AND is_active = 1 ORDER BY id DESC";
			$sql_pw = $this->db->query($sql_pw, array($id_user, $password));
			$sql_pw = $sql_pw->row_array();
			
			if ($sql_pw) {

				/*get user data*/
				$sql_us = $this->db->select('ms_user.*, ms_company.name company_name, tb_role.name role_name')
									->where('ms_user.id', $id_user)
									->where('ms_user.del', 0)
									->join('ms_company', 'ms_user.id_company = ms_company.id', 'LEFT')
									->join('tb_role', 'ms_user.id_role = tb_role.id')
									->get('ms_user');
				$sql_us = $sql_us->row_array();

				// print_r($id_user);
				// print_r($sql_us);die;
				if($sql_us){
				
					/*define User based on role*/
					$sql_role = "SELECT * FROM ms_user_role WHERE id = ?";
					$id_role  = $sql_us['id_role'];
					$sql_role = $this->db->query($sql_role, array($id_role));
					$sql_role = $sql_role->row_array();

					/*set the session*/
					$set_session = array(
						'id' 			=> 	$sql_us['id'], //ID USER
						'id_role'		=>	$sql_us['id_role'], // ID ROLE
						'id_company'	=>	$sql_us['id_company'], // ID COMPANY
						'role_name'		=>  $sql_role['role_name'], // NAME ROLE
						'company_name'	=>	$sql_us['company_name'], // COMPANY NAME
						'name'			=>	$sql_us['name'],
						'entry_stamp'	=>	$sql_us['entry_stamp'],
						'edit_stamp'	=>	$sql_us['edit_stamp'],
					);

					// SET SESSION LOGIN
					$this->session->set_userdata('admin',$set_session);

					#create LOG
					$log 		= fopen("log/ID_".$sql_us['id'].".txt", "a") or die("Unable to open file!");
					$time 		= date("Y-m-d H:i:s");
					$txt 		= $sql_us['id']."|Login|".$time."\n \r\n";
					fwrite($log, $txt);
					fclose($log);

				return "ok";
				}else{
					return false;
				}
			}else{

				/*in case user input the old password*/
				$sql_pw = "SELECT * FROM tr_password WHERE id_user = ? AND password = ? AND is_active = 0 ORDER BY id DESC LIMIT 1";

				$sql_pw = $this->db->query($sql_pw, array($id_user, $password));
				$sql_pw = $sql_pw->row_array();

				$sql_pw_old = $this->db->where('is_active', 1)->where('id_user', $id_user)->limit(1)->order_by('id', 'DESC')->get('tr_password');
				$sql_pw_old = $sql_pw_old->row_array();

				if ($sql_pw) {
					$msg = "<p class='error_msg animated bounceInDown'>Password diganti pada <b>".date_format(date_create($sql_pw_old['entry_stamp']), 'd M Y').".</b></p>";
					$this->session->set_flashdata('error_msg', $msg);
					return "oldpw";
				}
			}

		 // print_r($sql_un);print_r($sql_pw);die;
		}else{
			return false;
		}

	}

	function custom_query(){

		
		$query = "DELETE FROM ms_dokumen WHERE entry_by = 8";
		$this->db->query($query);
		$query = "DELETE FROM ms_berkas WHERE entry_by = 8";
		$this->db->query($query);
		// echo $this->db->last_query();
	}


	function getKurs(){
		$return = array();
		$query = "SELECT * FROM tb_kurs WHERE del = 0";
		$query = $this->db->query($query);
		foreach ($query->result_array() as $key => $value) {
			$return[$value['id']] = $value['symbol'];
		}
		return $return;
	}
}
