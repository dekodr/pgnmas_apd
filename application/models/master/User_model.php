<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model{
	public $table = 'ms_admin';
	function __construct(){
		parent::__construct();

	}
	function getData($form, $id=null){
		if($id==null){
			$user = $this->session->userdata('user');
		}else{
			$user['id_user'] = $id;
		}
		
		$query = "	SELECT  a.name,
							a.password,
							a.email,
							b.name role_name,
							a.id
					FROM ".$this->table." a JOIN tb_role b ON a.id_role = b.id WHERE del = 0";
		if($this->input->post('filter')){
			$query .= $this->filter($form, $this->input->post('filter'), false);
		}
		
		return $query;
	}

	function selectData($id){
		$query = "SELECT 	name,
							password,
							email,
							id_role
							FROM ".$this->table." WHERE id = ?";
		$query = $this->db->query($query, array($id));
		return $query->row_array();
	}
	function getRoleOption(){
		$query = "	SELECT id, name 
					FROM tb_role";
		$query = $this->db->query($query);

		$return = array();
		foreach($query->result_array() as $key => $row){
			$return[$row['id']] = $row['name'];
		}
		return $return;
	}


	public function insert_admin($data){
		$this->db->insert('ms_admin',$data);
		return $this->db->insert_id();
	}
	public function insert_data_admin($table, $data){
		return $this->db->insert($table,$data);	
	}
	public function update_admin($id, $data){
		$this->db->where('id', $id)->update('ms_admin',$data);

	}
	public function update_data_admin($id, $data){
		return $this->db->where('id_user', $id)->where('type', 'admin')->update('ms_login',$data);	
	}
}