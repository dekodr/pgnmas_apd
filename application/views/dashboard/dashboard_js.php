<script type="text/javascript">

 var optionsLine = {
    chart: {
        type: 'column',
         renderTo: 'graph',
    },
    title: {
        text: 'Rencana vs Realisasi Pengadaan'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'middle',
   
    },
    xAxis: {
        categories: [
            'Baseline',
            'Non-Baseline'
        ]
    },
    yAxis: {
        title: {
            text: 'Nilai'
        }
    },
   
   
   plotOptions: {
        column: {
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [{
        name: 'Rencana',
        data: [<?php echo $graph['total_rencana_baseline']?>, 0]
    }, {
        name: 'Realisasi',
        data: [<?php echo $graph['total_realisasi_baseline']?>,<?php echo $graph['total_realisasi_non_baseline']?>]
    }]
};
var chart = new Highcharts.Chart(optionsLine);


var optionsLine = {
    chart: {
        type: 'column',
         renderTo: 'graphs',
    },
    title: {
        text: 'Nilai Kontrak'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'middle',
   
    },
    xAxis: {
        categories: [
            'Baseline',
            'Non-Baseline',
            'Total'
        ]
    },
    yAxis: {
        title: {
            text: 'Nilai'
        },
         labels: {
             formatter: function () {

                console.log(this)
                        if (this.value.toFixed(0) >= 1000000000000) {
                            return this.value.toFixed(0) / 1000000000000 + 'T';
                        } else if (this.value.toFixed(0) >= 1000000000) {
                            return this.value.toFixed(0) / 1000000000 + 'M';
                        } else if (this.value.toFixed(0) >= 1000000) {
                            return this.value.toFixed(0) / 1000000 + 'Jt';
                        } else if(this.value.toFixed(0) >= 1000){
                            return this.value.toFixed(0) / 1000 + 'Rb';
                        }else if(this.value.toFixed(0)<0){
                            return '';
                        }else{
                                return this.value.toFixed(0);
                        }
                    }
        }
    },
   
    plotOptions: {
        column: {
            dataLabels: {
                enabled: true,
                formatter: function () {
                        if (this.y.toFixed(0) >= 1000000000000) {
                            return this.y.toFixed(0) / 1000000000000 + 'T';
                        } else if (this.y.toFixed(0) >= 1000000000) {
                            return Math.round((this.y.toFixed(0) / 1000000000) * 100) / 100 + 'M';
                        } else if (this.y.toFixed(0) >= 1000000) {
                            return this.y.toFixed(0) / 1000000 + 'Jt';
                        } else if(this.y.toFixed(0) >= 1000){
                            return this.y.toFixed(0) / 1000 + 'Rb';
                        }else if(this.y.toFixed(0)<0){
                            return '';
                        }else{
                                return this.y.toFixed(0);
                        }
                    }
            }

        }
    },  
    series: [{
         name: 'Nilai Rencana',
        data: [<?php echo $graph['total_nilai_baseline']?>, 0, <?php echo $graph['total_nilai_baseline']?>]
    },{
         name: 'Nilai Terkontrak',
        data: [<?php echo $graph['total_nilai_terkontrak_baseline']?>, <?php echo $graph['total_nilai_terkontrak_non_baseline']?>, <?php echo ($graph['total_nilai_terkontrak_baseline'] + $graph['total_nilai_terkontrak_non_baseline'])?>]
    }, {
        name: 'Nilai Terbayar',
        data: [<?php echo $graph['total_nilai_terbayar_baseline']?>, <?php echo $graph['total_nilai_terbayar_non_baseline']?>, <?php echo ($graph['total_nilai_terbayar_baseline'] + $graph['total_nilai_terbayar_non_baseline'])?> ]
    }]
};
var chart = new Highcharts.Chart(optionsLine);

<?php $admin    = $this->session->userdata('admin');?>
    var __role      = <?php echo $admin['id_role']?>;
    dataPost = {
        order: 'id',
        sort: 'desc'
    };
    var _xhr;
   
    var table1 = $('#tableGeneratorBaseline').tableGenerator({
        url: '<?php echo site_url('baseline_kontrak/dashboard/'.$id); ?>',
        data: dataPost,
        headers: [{
            "key"   : "no",
            "value" : "Rencana Pengadaan"
        },
        {
            "key"   : "action",
            "value" : "Action",
            "sort"  : false
        }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                var html = '';
               
                    html += '<a class="btn btn-success" href="'+site_url+"baseline_kontrak/view/"+data[1].value+'"><i class="fa fa-check"></i>&nbsp;Approve</a>';
                return html;
            },
            target : [1]

        }],
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });
    var table2 = $('#tableGeneratorKontrak').tableGenerator({
        url: '<?php echo site_url('kontrak/dashboard/'.$id); ?>',
        data: dataPost,
        headers: [{
            "key"   : "no",
            "value" : "Nomor Permintaan"
        },
        {
            "key"   : "issue_date",
            "value" : "Tanggal Permintaan"
        },
        {
            "key"   : "name",
            "value" : "Nama Paket Permintaan"
        },
        {
            "key"   : "action",
            "value" : "Action",
            "sort"  : false
        }],
        columnDefs : [{
            renderCell: function(data, row, key, el){
                return defaultDate(data[1].value)
                
            },
            target : [1]

        },{
            renderCell: function(data, row, key, el){
                var html = '';
                html += '<a class="btn btn-default " href="'+site_url+"kontrak/view/"+data[3].value+'"><i class="fa fa-search"></i>&nbsp;Lihat Data</a>';
                return html;
            },
            target : [3]
        }],
        filter: {
            wrapper: $('.contentWrap'),
            data : {
                data: _xhr
            }
        }
    });

</script>
