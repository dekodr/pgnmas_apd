<div class="mg-lg-12">

	<div class="block">
		<div>
			
			<table class="tableData">
				<tr>
					<td>
						Penyedia Barang &amp; Jasa : <?php echo $vendor['legal_name'].' '.$vendor['name'];?>
					</td>
					
				</tr>
				<?php if(isset($get_csms['csms_file'])){ ?>
				<tr>
					<td>
						Lampiran Sertifikat CSMS : <?php echo (isset($get_csms['csms_file'])) ? '<a href="'.base_url('lampiran/csms_file/'.$get_csms['csms_file']).'">Lampiran</a>': '-';?>
					</td>
				</tr>
				<tr>
					<td>
						Masa Berlaku : <?php echo ($get_csms['expiry_date'] == "lifetime") ? "Seumur Hidup" :( (strtotime($get_csms['expiry_date']) > 0) ? default_date($get_csms['expiry_date']) : "-");?>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td>
						Skor : <?php echo $get_csms['score'];?>
					</td>
				</tr>
				<tr>
					<td>
						Kategori : <?php echo $get_csms['value'];?>
					</td>
				</tr>
			</table>
			<div class="panel-group">
				<form action="<?php echo site_url('approval/csms/'.$id_vendor.'/1')?>" method="POST">
				<table id="scoreTable" class="scoreTable">
	            </table>
	           
				</form>
			</div>
		</div>
	</div>
</div>