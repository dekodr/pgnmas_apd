<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<title>{header}</title>

	<link rel="stylesheet" href="<?php echo base_url('assets/styles/normalize.css'); ?>" type="text/css" media="screen"/>
	<!-- <link rel="stylesheet" href="<?php echo base_url('assets/font/font-awesome/css/font-awesome.min.css'); ?>" type="text/css" media="screen"/> -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('assets/font/font/flaticon.css'); ?>" type="text/css" media="screen"/>
	<!-- <link rel="stylesheet" href="<?php echo base_url('assets/js/fullcalendar/fullcalendar.min.css'); ?>" type="text/css" media="screen"/> -->
	<link rel="stylesheet" href="<?php echo base_url('assets/js/datepicker/jquery.datetimepicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/clockpicker/src/clockpicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/clockpicker/dist/jquery-clockpicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/daterangepicker/jquery.comiseo.daterangepicker.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-ui/jquery-ui.min.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/base.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/main.min.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen"/>
	<script>
		var base_url = "<?php echo base_url()?>";
		var site_url = "<?php echo site_url()?>";
	</script>

	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
	<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>

	<script type="text/javascript" src="<?php echo base_url('assets/js/moment-with-locales.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui/jquery-ui.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.imask.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/tableGenerator_v2.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/folder_generator.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/filter.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/form.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/formWizard.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/modal.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/datepicker/build/jquery.datetimepicker.full.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/countdown/jquery.countdown.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/date.format.js')?>"></script>

	<script type="text/javascript" src="<?php echo base_url('assets/js/fullcalendar/fullcalendar.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/tinymce/js/tinymce/tinymce.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/clockpicker/src/clockpicker.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/common.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.js');?>"></script>

	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.number.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/numeral.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/utility.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/daterangepicker/jquery.comiseo.daterangepicker.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui/ui/jquery.ui.tooltip.min.js');?>"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.actual/1.0.19/jquery.actual.js"></script>
	<!-- Datejs-master/build/date.js -->
	<script src="https://code.highcharts.com/highcharts.js"></script>

	<script type="text/javascript" src="<?php echo base_url('assets/js/Datejs-master/build/date.js');?>"></script>
	<style>
		.pull-right {float: right;}
	</style>

</head>

<body>



<div id="container">

	<nav class="navbar">

		<div class="navbar-brand">
			
			<div class="navbar-item logo">

				<img src="<?php echo base_url('assets/images/img/Logo_Apd_Permata-01.png')?>">

			</div>

		</div>

		<div class="navbar-menu">

			<div class="navbar-start">
				
				<div class="navbar-item">
					<div class="search-bar">
						<input type="text" name="catalog" class="input" placeholder="Search..">
						<span class="icon">
							<i class="fa fa-search"></i>
						</span>
					</div>
				</div>

			</div>

			<div class="navbar-end">

				<div class="navbar-item account has-dropdown">

					<img src="<?php echo base_url('assets/images/Man-Avatar.png')?>" alt="" height="45px">

          			<p>{user}</p>

          			<span class="icon spin"><i class="fa fa-angle-down"></i></span>

          			<div class="navbar-dropdown is-dropdown">

		            	<a href="<?php echo site_url('main/logout')?>" class="navbar-item">

		              		<span class="icon"><i class="fas fa-sign-out-alt"></i></span>

		              		Logout

		            	</a>

		          	</div>

				</div>

			</div>

		</div>
	</nav>

	<section class="contentWrap">
		<div class="hbox">

			<div class="sidebar">

				{sideMenu}

			</div>

			<div class="main">
				
				<div class="mainWrapper">
						<!-- <div class="sr-wrapper">
							
							
						</div> -->
						<button style="display: none" class="button sr-close-btn is-danger">Close all</button>
					<?php

						if($this->session->userdata('alert'))

					?>

					{breadcrumb}

					<?php

						echo $this->session->userdata('msg');

					?>

					<h1 class="page-heading">{header}</h1>

					<div class="row">

						{content}

					</div>
					
				</div>

			</div>

		</div>

	</section>

</div>

<div class="form-keterangan-reject">
	<span class="fkr-btn-close">
		<i class="fas fa-times"></i>
	</span>
	<div class="fkr-content">
		<fieldset class="form-group" for="" style="display: block;">
			<label for="keterangan">Keterangan</label>
			<textarea type="text" class="form-control fkr-textarea" id="" value="" name="" placeholder="isi keterangan penolakan"></textarea>
		</fieldset>
	</div>
	<div class="fkr-btn-group">
		<button class="is-danger" type="submit" name="reject">Reject</button>
	</div>
</div>
</body>
	{script}
	<script type="text/javascript">
	$(function() {
		$('.search-bar input[name="catalog"]').keyup(function() {
				_self = $(this);
			// _wrapper 		= $('mainWrapper').append('')
			_parent 		= _self.closest('.input');
			_resultWrapper 	= $('.content-right');
			_resultWrapper.empty();

			

		$('.sr-close-btn').click(function() {
			$('.sr-wrapper').removeClass('active');
			$(this).css('display','none');
		})
	})
</script>
</html>
