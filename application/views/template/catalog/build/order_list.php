<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/main.min.css'); ?>" />
    <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen"/>
</head>

<body>

  <nav class="navbar">

    <div class="navbar-brand">

      <div class="navbar-item logo">

        <img src="../source/img/Logo_Apd_Permata-01.png" alt="">

      </div>

     <div class="navbar-item" style="display: none">

        <a role="button" class="navbar-burger has-sidebar">

          <span aria-hidden="true"></span>

          <span aria-hidden="true"></span>

          <span aria-hidden="true"></span>

        </a>

     </div>

    </div>

    <div class="navbar-menu">

      <div class="navbar-start">

        <div class="navbar-item">

          <div class="search-bar">

            <span class="icon"><i class="fa fa-search"></i></span>

            <input type="text" class="input" placeholder="Search">

          </div>

        </div>

      </div>

      <div class="navbar-end">

        <div class="navbar-item account has-dropdown">

          <img src="../source/img/man-avatar.png" alt="">

          <p>Account</p>

          <span class="icon"><i class="fa fa-angle-down"></i></span>

          <div class="navbar-dropdown is-dropdown">

            <a href="layout-login.php" class="navbar-item">

              <span class="icon"><i class="fas fa-sign-out-alt"></i></span>

              Logout

            </a>

          </div>

        </div>

      </div>

    </div>

  </nav>

  <section class="main-content"> 

    <div class="wrapper">

      <div class="col col-2">
        
        <?php include "_sidebar.php" ?>

      </div>

      <div class="col col-10">
        
        <div class="content" id="content" oncontextmenu="return false;">

          <div class="wrapper">

            <div class="col col-12">

              	<ol class="breadcrumb" aria-label="breadcrumbs">

		            <li><a href="#">Dashboard</a></li>

		        </ol>

            </div>

				<div class="col col-12">

					<div class="panel" style="display: flex; justify-content: center; flex-wrap: wrap;">

						<div class="container-title" style="width: 100%;">
			              <h3>Here is Table</h3>
			              <button class="is-primary pull-right">Laporan</button>
			            </div>

			            <table class="table">
			            	<thead>
			            		<tr>
				            		<th>#</th>
				            		<th>Nama Barang</th>
				            		<th>Deskripsi</th>
				            		<th>Perkiraan Harga</th>
				            	</tr>
			            	</thead>
			            	<tr>
			            		<td>998</td>
			            		<td>Sepatu</td>
			            		<td>Barang warna coklat</td>
			            		<td>
			            			Rp. 443.000
			            		</td>
			            	</tr>
			            	<tr>
			            		<td>997</td>
                      <td>Helm</td>
                      <td>Barang warna kuning</td>
                      <td>
                        Rp. 143.000
                      </td>
			            	</tr>
			            </table>

	         		</div>

				</div>

          </div>

        </div>

      </div>

    </div>

    <div class="bg-shape">
    	<img src="../source/img/bg-shape.png" alt="">
    </div>

  </section>

	<?php include "_js.php" ?>

</body>

</html>