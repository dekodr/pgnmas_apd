<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cart - APD</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/catalog-main.css'); ?>" />
    <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen"/>

    <link rel="stylesheet" href="../source/vendors/joyride/joyride-1.0.css">
    <link rel="stylesheet" href="demo-style.css">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
    <!-- <link href="justified-nav.css" rel="stylesheet" type="text/css"/> -->
    <!-- <link href="../source/vendors/hemi/src/jquery.hemiIntro.css" rel="stylesheet" type="text/css"/>

    <style>
      .popover {
         width: 200px;
      }
      .popover-content {
        background-color: #fff;
        border-radius: 4px;
        box-shadow: 0 2px 6px rgba(0,0,0,.2);
        padding: 5px 15px;  
      }

      .btn {display: none;}
    </style> -->

</head>
<body>

	  <?php include "_nav.php" ?>

    <section class="cart-wrapper">
      <!-- form start here -->
      <div class="content-left">
        <div class="content-title">
          Shopping Cart
        </div>
        <div class="cart-list-wrapper">

          <div class="list-group">
            <div class="group-title">
              Group of
            </div>
            <ul>
              <li class="cart-list">
                <div class="product-img" style="background-image: url('<?php echo base_url("assets/images/products/boots-1.png") ?>')">
                  
                </div>
                <div class="product-name">
                  Product Name
                  <input type="file" id="numero2">
                </div>
                <div class="amount">
                  <button id="numero3">
                    <i class="fas fa-minus"></i>
                  </button>
                  <input type="text" class="amount-text" readonly>
                  <button>
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
                <div class="product-price">
                  Rp. 300.000
                </div>
                <div class="delete-button">
                  <span class="icon">
                    <i class="fas fa-times"></i>
                  </span>
                </div>
              </li>
              <li class="cart-list">
                <div class="product-img" style="background-image: url('<?php echo base_url("assets/images/products/earplug-1.png") ?>')">
                  
                </div>
                <div class="product-name">
                  Product Name
                  <input type="file">
                </div>
                <div class="amount">
                  <button>
                    <i class="fas fa-minus"></i>
                  </button>
                  <input type="text" class="amount-text" readonly>
                  <button>
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
                <div class="product-price">
                  Rp. 300.000
                </div>
                <div class="delete-button">
                  <span class="icon">
                    <i class="fas fa-times"></i>
                  </span>
                </div>
              </li>
            </ul>
          </div>

          <div class="list-group">
            <div class="group-title">
              Group of
            </div>
            <ul>
              <li class="cart-list">
                <div class="product-img" style="background-image: url('<?php echo base_url("assets/images/products/googles-1.png") ?>')">
                  
                </div>
                <div class="product-name">
                  Product Name
                  <input type="file">
                </div>
                <div class="amount">
                  <button>
                    <i class="fas fa-minus"></i>
                  </button>
                  <input type="text" class="amount-text" readonly>
                  <button>
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
                <div class="product-price">
                  Rp. 300.000
                </div>
                <div class="delete-button">
                  <span class="icon">
                    <i class="fas fa-times"></i>
                  </span>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="cart-back-button">
          <button>
            <i class="fas fa-arrow-left"></i>
            Lanjutkan Belanja 
          </button>

          <div class="sub-total" style="display: none">
            <span>Subtotal</span> Rp. 900.000
          </div>
        </div>
      </div>
      <div class="content-right">
        <div class="card-detail">
          <div class="title">
            Detail
          </div>

          <div class="notification-detail">
            Upload logo yang akan diaplikasikan pada barang anda, minimal ukuran 500kb
          </div>

          <div class="sub-total">
            <span>Subtotal</span> Rp. 900.000
          </div>

          <div class="checkout-button joyride" id="numero1">
            <button class="tg" onclick="location.href='status_pemesanan.php'">
              Checkout
              <!-- <span class="tooltiptext bottom active">Click here to pay</span> -->
              <label class="toggleButton" style="display: none">
                  <input type="checkbox" class="input" checked="checked">
                  <div>
                      <svg viewBox="0 0 44 44">
                          <path d="M14,24 L21,31 L39.7428882,11.5937758 C35.2809627,6.53125861 30.0333333,4 24,4 C12.95,4 4,12.95 4,24 C4,35.05 12.95,44 24,44 C35.05,44 44,35.05 44,24 C44,19.3 42.5809627,15.1645919 39.7428882,11.5937758" transform="translate(-2.000000, -2.000000)"></path>
                      </svg>
                  </div>
              </label>
            </button>
          </div>
        </div>
      </div>
      <!-- form end here -->
    </section>

    <section class="signature">

      <div class="left">

        brought to you by :

        <img src="../source/img/pgn_mas.png" alt="">

      </div>

      <div class="center">

        operated by :

        <img src="../source/img/kpusahatama.png" alt="">

      </div>

      <div class="right">

        E-Commerce Platform by :

        <img src="../source/img/dekodr.png" alt="">

      </div>

    </section>

    <!-- <ol id="joyRideTipContent">
      <li data-id="numero3">
        <p>Masukan jumlah Barang</p>
      </li>
      <li data-id="numero2" data-text="Next">
        <p>Upload Logo</p>
      </li>
      <li data-id="numero1" data-text="Close">
        <p>Klik disini untuk Proses Pemesanan</p>
      </li> -->
      <!-- <li data-id="numero2" data-text="Next">
        <h2>Stop #2</h2>
        <p>Click here to go back to home</p>
      </li>
      <li data-id="numero1" data-text="Close">
        <h2>Stop #3</h2>
        <p>Now what are you waiting for? Add this to your projects and get the most out of your apps!</p>
      </li> -->
    </ol>

  <script type="text/javascript" src="../source/js/vendors/jquery-3.3.1.js">
  </script>
  <script type="text/javascript" src="../source/js/vendors/jquery-ui.js">
  </script>
  <script type="text/javascript" src="../source/js/app.js"></script>

  <script type="text/javascript" src="../source/vendors/joyride/jquery-1.6.2.min.js"></script>
  <script type="text/javascript" src="../source/vendors/joyride/jquery.joyride-1.0.js"></script>

  <script type="text/javascript">
    $(window).load(function() {
      $("#joyRideTipContent").joyride();
    });
  </script>

 <!--  <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script src="../source/vendors/hemi/src/jquery.hemiIntro.js" type="text/javascript"></script>

  <script type="text/javascript">
        //
        $(function () {
          var intro = $.hemiIntro({
            debug: false,
            steps: [
              {
                selector: ".navbar",
                placement: "bottom",
                content: "This is Navbar",
              },
              {
                selector: ".content-right",
                placement: "top",
                content: "Text2 text2 text2 text2 text2 text2 text2 text2 text2 text2 text2 text2",
                offsetTop: 100
              },
              {
                selector: ".step-3",
                placement: "right",
                content: "Text3 text3 text3 text3 text3 text3 text3"
              },
            ],
            startFromStep: 0,
            backdrop: {
              element: $("<div>"),
              class: "hemi-intro-backdrop"
            },
            popover: {
              template: '<div class="popover hemi-intro-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
            },
            // buttons: {
            //   holder: {
            //     element: $("<div>"),
            //     class: "hemi-intro-buttons-holder"
            //   },
            //   next: {
            //     element: $("<button>Next</button>"),
            //     class: "btn btn-primary"
            //   },
            //   finish: {
            //     element: $("<button>Finish</button>"),
            //     class: "btn btn-primary"
            //   }
            // },
            welcomeDialog: {
              show: true,
              selector: "#myModal"
            },
            scroll: {
              anmationSpeed: 500
            },
            currentStep: {
              selectedClass: "hemi-intro-selected"
            },
            init: function (plugin) {
              console.log("init:");
            },
            onLoad: function (plugin) {
              console.log("onLoad:");
            },
            onStart: function (plugin) {
              console.log("onStart:");
            },
            onBeforeChangeStep: function () {
              console.log("onBeforeChangeStep:");
            },
            onAfterChangeStep: function () {
              console.log("onAfterChangeStep:");
            },
            onShowModalDialog: function (plugin, modal) {
              console.log("onShowModalDialog:");
            },
            onHideModalDialog: function (plugin, modal) {
              console.log("onHideModalDialog:");
            },
            onComplete: function (plugin) {
              console.log("onComplete:");
            }
          });
          intro.start();
        });
        //-->
  </script>

  <script>

    // Auto animate for demo

  </script>
   

</body>
</html>