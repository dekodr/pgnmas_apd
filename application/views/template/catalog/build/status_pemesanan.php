<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/main.min.css'); ?>" />
    <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen"/>
</head>

<body>

  <nav class="navbar">

    <div class="navbar-brand">

      <div class="navbar-item logo">

        <img src="<?php echo base_url("assets/images/img/Logo_Apd_Permata-02.png") ?>" alt="">

      </div>

     <div class="navbar-item" style="display: none">

        <a role="button" class="navbar-burger has-sidebar">

          <span aria-hidden="true"></span>

          <span aria-hidden="true"></span>

          <span aria-hidden="true"></span>

        </a>

     </div>

    </div>

    <div class="navbar-menu">

      <div class="navbar-start">

        <div class="navbar-item">

          <div class="search-bar">

            <input type="text" class="input" placeholder="Search">

            <span class="icon"><i class="fa fa-search"></i></span>

          </div>

        </div>

      </div>

      <div class="navbar-end">

        <div class="navbar-item account has-dropdown">

          <img src="../source/img/man-avatar.png" alt="">

          <p>Account</p>

          <span class="icon"><i class="fa fa-angle-down"></i></span>

          <div class="navbar-dropdown is-dropdown">

            <a href="layout-login.php" class="navbar-item">

              <span class="icon"><i class="fas fa-sign-out-alt"></i></span>

              Logout

            </a>

          </div>

        </div>

      </div>

    </div>

  </nav>

  <section class="main-content"> 

    <div class="wrapper">

      <div class="col col-2">
        
        <?php include "_sidebar.php" ?>

      </div>

      <div class="col col-10">
        
        <div class="content" id="content" oncontextmenu="return false;">

          <div class="wrapper">

            <div class="col col-12">

              	<ol class="breadcrumb" aria-label="breadcrumbs">

		            <li><a href="#">Dashboard</a></li>

		        </ol>

            </div>

      				<div class="col col-12">

      					<div class="panel">

    						  <div class="container-title" style="width: 100%;">
			              <h3>PO #001</h3>
			            </div>

			            <div class="step-wrapper">
                 
                    <ul>
                      <li class="active">
                        <div class="step">
                          <div class="step-icon">
                            <img src="<?php echo base_url("assets/images/img/003.svg") ?>" alt="">
                          </div>
                          <div class="step-title">
                            Pesananan <br> Diterima
                          </div>
                        </div>
                      </li>
                      <li class="active">
                        <div class="step">
                          <div class="step-icon">
                            <img src="<?php echo base_url("assets/images/img/004.svg") ?>" alt="">
                          </div>
                          <div class="step-title">
                            Purchased <br> Order
                          </div>
                        </div>
                      </li>
                      <li class="denied">
                        <div class="step">
                          <div class="step-icon">
                            <img src="<?php echo base_url("assets/images/img/001.svg") ?>" alt="">
                          </div>
                          <div class="step-title">
                            Delivery <br> Order
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="step">
                          <div class="step-icon">
                            <img src="<?php echo base_url("assets/images/img/002.svg") ?>" alt="">
                          </div>
                          <div class="step-title">
                            Pesanan <br> Sampai
                          </div>
                        </div>
                      </li>
                    </ul>

                  </div>

                  <div class="container-title" style="width: 100%;">
                    <h3>List Item</h3>
                  </div>

                  <div class="list-wrapper">
                    
                    <ul>
                      
                      <li>

                        <div class="list-item">
                      
                          <div class="list-image" style="background-image: url('<?php echo base_url("assets/images/earplug-png/ep-4.png") ?>')">
                            
                          </div>

                          <div class="list-attr">

                            <div class="list-name">
                              Item 001
                            </div>
                              
                            <div class="list-caption">

                              <span class="amount">x3 (satuan)</span>

                              <span class="total">
                              Total : Rp. 900.000,00
                              </span>

                            </div>
                        
                          </div>

                        </div>

                      </li>

                      <li>

                        <div class="list-item">
                      
                          <div class="list-image" style="background-image: url('<?php echo base_url("assets/images/boots-png/s-1.png") ?>')">
                            
                          </div>

                          <div class="list-attr">

                            <div class="list-name">
                              Item 001
                            </div>
                              
                            <div class="list-caption">

                              <span class="amount">x3 (satuan)</span>

                              <span class="total">
                              Total : Rp. 900.000,00
                              </span>

                            </div>
                        
                          </div>

                        </div>

                      </li>

                      <li>

                        <div class="list-item">
                      
                          <div class="list-image" style="background-image: url('<?php echo base_url("assets/images/googles-png/googles-2.png") ?>')">
                            
                          </div>

                          <div class="list-attr">

                            <div class="list-name">
                              Item 001
                            </div>
                              
                            <div class="list-caption">

                              <span class="amount">x3 (satuan)</span>

                              <span class="total">
                              Total : Rp. 900.000,00
                              </span>

                            </div>
                        
                          </div>

                        </div>

                      </li>

                    </ul>

                  </div>

  	         		</div>

      				</div>

          </div>

        </div>

      </div>

    </div>

    <div class="bg-shape">
      <img src="../source/img/bg-shape.png" alt="">
    </div>

  </section>

	<?php include "_js.php" ?>

</body>

</html>