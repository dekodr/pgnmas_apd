    <nav class="navbar">

      <div class="navbar-brand">

        <div class="navbar-item logo" onclick="location.href='index.php'">

          <div class="logo-bg"></div>

          <img src="<?php echo base_url('assets/images/img/Logo_Apd_Permata-02.png')?>">

        </div>

      </div>

      <div class="navbar-menu">

        <div class="navbar-start">

          <div class="navbar-item">

            <div class="search-bar">

              <!-- <span class="icon"><i class="fa fa-search"></i></span> -->

              <input type="text" class="input" placeholder="Search">

              <span class="icon search" onclick="#">

                  <i class="fa fa-arrow-right"></i>

                  <i class="fa fa-search"></i>

              </span>

            </div>

          </div>

        </div>

        <div class="navbar-end">

          <div class="navbar-item">

            <a href="permata-catalog.php">

              <span class="hover hover-1">Home</span>

            </a>

            <!-- <span class="tooltiptext bottom active">Click here to pay</span> -->

          </div>

          <div class="navbar-item">

            <a href="permata-cart.php" class="button-cart shop">

              <span class="icon"><i class="fas fa-shopping-cart"></i></span>

              <span class="text">Cart</span>

              <span class="cart-count">3</span>

            </a>

          </div>

        </div>

      </div>

    </nav>