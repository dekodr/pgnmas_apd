	<script type="text/javascript" src="../source/js/vendors/jquery-3.3.1.js">
	</script>
	<script type="text/javascript" src="../source/js/vendors/jquery-ui.js">
	</script>
	<script type="text/javascript" src="../source/js/app.js"></script>
	<script>
		$(document).ready(function() {
			$(".has-child").click(function() {
				$(this).children(".is-dropdown").toggleClass("active");
				$(this).children(".text").find(".icon").toggleClass("active");
				$(this).toggleClass("active");
			});

			$('.caret').click(function() {
				$(this).siblings('.nested').toggleClass('active');
				$(this).siblings('.icon').toggleClass('active');
			})
		})
	</script>