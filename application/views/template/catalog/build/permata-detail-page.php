<!DOCTYPE html>
<html lang="en">
<head>
    <title>Detail - APD</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/catalog-main.css'); ?>" />
    <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen"/>
</head>
<body>

	  <?php include "_nav.php" ?>

    <section class="header">

      <div class="overlay"></div>

      <div class="header-title">

        Katalog <span>Produk</span>

        <div class="span-line"></div>

      </div>

      <div class="breadcrumb">
        <a href="permata-catalog.php">
          <span>Kategori Produk</span>
        </a>
        <a href="#">
          <span class="icon"><i class="fas fa-angle-right"></i></span>
        </a>
        <a href="permata-catalog.php">
          <span>Nama Produk</span>
        </a>
      </div>

    </section>

    <section class="container">

      <div class="wrapper" style="align-items: center; justify-content: center;">

        <div class="detail">
          
          <div class="detail-left" style="background-image: url('<?php echo base_url("assets/images/products/boots-1.png") ?>')">
            
          </div>

          <div class="detail-right">
            
            <div class="detail-info"> <br>
              
              <span class="detail-title">Title Here</span> <br>

              <span class="price">Rp. 30.000</span>

              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti quasi repellendus sit, corrupti cumque illo, nam suscipit ullam molestias libero eius. Voluptatibus autem officiis molestias repellat aspernatur! Dicta, autem, officiis.
              </p>

              <div class="amount">
                <select name="" id="">
                  <?php for ($i=1; $i <= 10 ; $i++) { ?>
                    <option value=""><?= $i ?></option>
                  <?php } ?>
                </select>

                <a href="#" class="button-cart shop checkout">

                  <span class="icon"><i class="fas fa-shopping-cart"></i></span>

                  <span class="text">Tambah ke Keranjang</span>

                </a>

                <a href="#" class="button-cart shop checkout is-link beli-langsung">

                  <span class="icon"><i class="fas fa-shopping-cart"></i></span>

                  <span class="text">Beli Langsung</span>

                </a>
            
              </div>

              <div class="image-preview">

                <img src="<?php echo base_url('assets/images/products/boots-1.png') ?>" alt="" onclick="img_change(1)">

                <img src="<?php echo base_url('assets/images/products/boots-2.png') ?>" alt="" onclick="img_change(2)">

                <img src="<?php echo base_url('assets/images/products/boots-3.png') ?>" alt="" onclick="img_change(3)">

                <img src="<?php echo base_url('assets/images/products/boots-4.png') ?>" alt="" onclick="img_change(4)">

                <img src="<?php echo base_url('assets/images/products/boots-5.png') ?>" alt="" onclick="img_change(5)">

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

    <section class="related-product">

      <div class="wrapper">
        
        <span class="related-title">
          Produk <span>Serupa</span>
        </span>

        <div class="card-wrapper">

          <?php for ($i=1; $i <= 4 ; $i++) { ?>

            <div class="card" style="background-image: url('<?php echo base_url("assets/images/earplug-png/ep-4.png") ?>')" onclick="location.href='permata-detail-page.php'">

              <div class="card-header"></div>

              <span class="icon shop"><i class="fas fa-shopping-cart"></i></span>

              <div class="card-title">

                Here Title

                <span>Sub Title</span>

                <span class="price">Rp. 30.000</span>

              </div>

            </div>
            
          <?php } ?>

        </div>

      </div>
      
    </section>

    <section class="footer">

      <div class="overlay"></div>

      <div class="footer-info">

        <div class="footer-left">

          <div class="info-header" style="height: 45px">

            <!-- <img src="../source/img/logo.jpg" alt=""> -->

          </div>

          <p>

            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, voluptatum enim excepturi dolores, quam fuga architecto quae quis laboriosam provident aliquid corporis repellendus quisquam cupiditate. Veritatis nobis necessitatibus vel exercitationem.

          </p>

          <a href="#" class="link">

            <span class="icon"><i class="fas fa-globe-asia"></i></span>

            <span class="hover hover-1">http://www.loremipsum.com</span>

          </a>

        </div>

        <div class="footer-right">

          <div class="footer-box-page">

            <span class="box-title">Akun <span>Saya</span></span>

            <ul>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Personal Info</span> 

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Order Saya</span> 

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Keranjang Belanja</span> 

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Wishlist</span> 

                </a>

              </li>

            </ul>

          </div>

          <div class="footer-box-page">

            <span class="box-title">Permat<span>apd</span> </span>

            <ul>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Tentang Kami</span> 

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Jasa Kami</span> 

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Benefit</span> 

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Layanan</span>

                </a>

              </li>

              <li>

                <a href="#">

                  <span class="icon"><i class="fas fa-angle-right"></i></span>

                  <span class="hover hover-1">Kontak Kami</span> 

                </a>

              </li>

            </ul>

          </div>

        </div>

      </div>
      
    </section>

    <section class="signature">

      <div class="left">

        brought to you by :

        <img src="../source/img/pgn_mas.png" alt="">

      </div>

      <div class="center">

        operated by :

        <img src="../source/img/kpusahatama.png" alt="">

      </div>

      <div class="right">

        E-Commerce Platform by :

        <img src="../source/img/dekodr.png" alt="">

      </div>

    </section>

    <div class="modal-wrapper">
      
      <div class="modal-checkout">

        <div class="button-close">
          <span class="icon">
            <i class="fas fa-times"></i>
          </span>
        </div>
      
        <div class="modal-left">
          <div class="item-name">
            <div class="product-img"></div>
            Item Name
          </div>
          <div class="item-spec">
            <ul>
              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
              <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
            </ul>
          </div>
          <div class="upload-file">
            <span>*Upload logo yang akan diaplikasikan pada barang anda, maximal ukuran 500kb</span>
            <input type="file">
          </div>
        </div>

        <div class="modal-right">
          <div class="item-name">
            Detail
          </div>
          <div class="item-spec">
            Harga
            <span>Rp. 300.000</span>
          </div>
          <div class="item-spec">
            Jumlah
            <span>
              <input type="number">
              (satuan)
            </span>
          </div>
          <div class="line-plus">
            <span class="line"></span>
            +
          </div>
          <div class="item-spec bottom">
            Total
            <span>Rp. 900.000</span>
          </div>
          <div class="button-pay">
            <button>Pay</button>
          </div>
        </div>

    </div>

    </div>

  <script type="text/javascript" src="../source/js/vendors/jquery-3.3.1.js">
  </script>
  <script type="text/javascript" src="../source/js/vendors/jquery-ui.js">
  </script>
  <script type="text/javascript" src="../source/js/app.js"></script>

  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.js');?>"></script>

  <script>
    $(document).ready(function() {
      $('.beli-langsung').click(function() {
        $('.modal-wrapper').toggleClass('active');
      });
      $('.button-close').click(function() {
        $('.modal-wrapper').removeClass('active');
      })
    })

    function img_change(param) {
      $('.detail-left').css("background-image","url('../../assets/images/products/boots-"+param+".png')")
    }
  </script>
   

</body>
</html>