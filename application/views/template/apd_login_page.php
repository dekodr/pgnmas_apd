<!DOCTYPE html>
<html lang="en">
<head>
    <title>Welcome to PGNMAS - APD</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/styles/scss/main.min.css" />

    <style>
		body {
		   margin: 0;
		   height: 100%;
		   display: flex;
		}

		canvas {
			position: fixed;
			z-index: 0;
		} 
	</style>
</head>
<body id="punk">

	<div class="bg" id="punk">
		
	</div>

	<div class="login-wrapper">

		<canvas></canvas>
		
		<div class="left-side">
			
			<div class="title">

				PERMAT <span class="bold">APD</span>

				<span class="sub-title">Never seen this before? then you will see</span>

			</div>

			<div class="form-wrapper">
				
				<form action="#" class="form">

					<fieldset class="form-control">
						<label for="#">Username</label>
						<input type="text">
					</fieldset>
					<fieldset class="form-control">
						<label for="#">Password</label>
						<input type="text">
					</fieldset>
					<div class="add-on">
						<div class="checkbox">
							<input type="checkbox">
							Remember me
						</div>
						<div class="link">
							<a href="#">Forgot password</a>
						</div>
					</div>
					<div class="button-group">
						<button class="btn-hover color-1">log in</button>
					</div>

				</form>

			</div>

		</div>

		<div class="right-side">

			<!-- <img src="../source/img/feather.png" alt="" class="logo"> -->
			
		</div>

	</div>

	<?php include "_js.php" ?>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>

	<script>
		let APP;
		document.addEventListener('DOMContentLoaded', init, false);

		function init () {
		   APP = new App();
		   events();
		   loop();
		}

		function loop () {
		   APP.render();
		   requestAnimationFrame(loop);
		}

		function events () {
		   document.addEventListener('mousemove', APP.mousemoveHandler, false);
		   document.addEventListener('mouseleave', APP.mouseleaveHandler, false);
		   window.addEventListener('resize', APP.resize, false);
		}

		class App {
		   constructor () {
		      this.canvas = document.querySelector('canvas');
		      this.context = this.canvas.getContext('2d');
		      this.canvas.width = this.width = window.innerWidth;
		      this.canvas.height = this.height = window.innerHeight;
		      
		      this.setupDots();
		      
		      this.resize = this.resize.bind(this);
		      this.mousemoveHandler = this.mousemoveHandler.bind(this);
		      this.mouseleaveHandler = this.mouseleaveHandler.bind(this);
		   }
		   
		   setupDots () {
		      this.dots = [];
		      this.scl = 30;
		      this.cols = this.width / this.scl;
		      this.rows = this.height / this.scl;
		      
		      let id = 0;
		      for (let x = 0; x < this.cols; x += 1) {
		         for (let y = 0; y < this.rows; y += 1) {
		            this.dots.push(new Dot(id, x * this.scl, y * this.scl, this.context, this.scl));
		            id += 1;
		         }
		      }
		   }
		   
		   resize () {
		      this.canvas.width = this.width = window.innerWidth;
		      this.canvas.height = this.height = window.innerHeight;
		      this.setupDots();
		   }
		   
		   mousemoveHandler (event) {
		      this.dots.forEach(d => {
		         d.mousemove(event);
		      })
		   }
		   
		   mouseleaveHandler () {
		      this.dots.forEach(d => {
		         d.isHover = false;
		      })
		   }
		   
		   render () {
		      this.context.clearRect(0, 0, this.width, this.height);
		      
		      this.dots.forEach(d => {
		         d.render();
		      })
		   }
		}

		class Dot {
		   constructor (id, x, y, context, scl) {
		      this.id = id;
		      this.x = x;
		      this.y = y;
		      this.new = { x: x, y: y, radius: 3, color: 'rgba(229, 249, 42, 0)' };
		      this.radius = 3;
		      
		      this.context = context;
		      this.scl = scl;
		      this.isHover = false;
		      this.isANimated = false;
		   }
		   
		   mousemove (event) {
		      const x = event.clientX;
		      const y = event.clientY;
		      
		      this.isHover = ((Math.abs(this.x - x) < (this.scl / 4 * 9)) && (Math.abs(this.y - y) < (this.scl / 4 * 9))) ? true : false;
		      this.isCenter = ((Math.abs(this.x - x) < (this.scl / 4 * 5)) && (Math.abs(this.y - y) < (this.scl / 4 * 5))) ? true : false;
		      this.isClosest = ((Math.abs(this.x - x) < (this.scl / 4 * 2)) && (Math.abs(this.y - y) < (this.scl / 4 * 2))) ? true : false;
		      
		      if (this.isHover && !this.isCenter && !this.isClosest) {
		         TweenMax.to(this.new, 0.4, {
		            radius: 5
		         });
		      } else if (this.isHover && this.isCenter) {
		         TweenMax.to(this.new, 0.4, {
		            radius: this.isClosest ? 14 : 9
		         });
		      } else {
		         TweenMax.to(this.new, 0.4, {
		            radius: 3
		         });
		      }
		   }
		   
		   render () {
		      this.context.beginPath();
		      this.context.arc(this.new.x, this.new.y, this.new.radius, 0, 2 * Math.PI, false);
		      this.context.fillStyle = this.new.color;
		      this.context.fill();
		   }
		}
	</script>

	<script>
  		$(document).ready(function() {
  			var lFollowX = 0,
			    lFollowY = 0,
			    x = 0,
			    y = 0,
			    friction = 1 / 30;

			function moveBackground() {
			  x += (lFollowX - x) * friction;
			  y += (lFollowY - y) * friction;
			  
			  translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

			  $('.bg').css({
			    '-webit-transform': translate,
			    '-moz-transform': translate,
			    'transform': translate
			  });

			  window.requestAnimationFrame(moveBackground);
			}

			$(window).on('mousemove click', function(e) {

			  var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
			  var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
			  lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
			  lFollowY = (10 * lMouseY) / 100;

			});

			moveBackground();
  		})
  	</script>
</body>

</html>