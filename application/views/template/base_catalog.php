<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{header}</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="stylesheet" href="<?php echo base_url('assets/styles/scss/catalog-main.css'); ?>" />
        <!-- <link rel="stylesheet" href="assets/css/vendors/jquery-ui.css" /> -->
        <link rel="stylesheet" href="<?php echo base_url('assets/styles/fontawesome5.6.3/css/all.css'); ?>" type="text/css" media="screen"/>
    </head>
    <body>
        <!-- HEADER MENU-->
        <nav class="navbar">

            <div class="navbar-brand">

                <div class="navbar-item logo" onclick="location.href='index.php'">

                <div class="logo-bg"></div>

                <img src="<?php echo base_url('assets/images/img/Logo_Apd_Permata-02.png')?>">

                </div>

            </div>

            <div class="navbar-menu">

                <div class="navbar-start">

                <div class="navbar-item">

                    <div class="search-bar">

                    <!-- <span class="icon"><i class="fa fa-search"></i></span> -->

                    <input type="text" class="input" placeholder="Search">

                    <span class="icon search" onclick="#">

                        <i class="fa fa-arrow-right"></i>

                        <i class="fa fa-search"></i>

                    </span>

                    </div>

                </div>

                </div>

                <div class="navbar-end">

                <div class="navbar-item">

                    <a href="permata-catalog.php">

                    <span class="hover hover-1">Home</span>

                    </a>

                    <!-- <span class="tooltiptext bottom active">Click here to pay</span> -->

                </div>

                <div class="navbar-item">

                    <a href="permata-cart.php" class="button-cart shop">

                    <span class="icon"><i class="fas fa-shopping-cart"></i></span>

                    <span class="text">Cart</span>

                    <span class="cart-count">3</span>

                    </a>

                </div>

                </div>

            </div>

        </nav>

        <!-- HEADER CONTENT -->
        <section class="header">
            <div class="overlay"></div>

            <div class="header-title">
                {headertitle}
                <div class="span-line"></div>
            </div>
        </section>

        <!-- MAIN AREA -->
        <section class="container">
            {content}
        </section>
        
        <!-- FOOTER AREA -->
        <section class="footer">

            <div class="overlay"></div>

            <div class="footer-info">

                <div class="footer-left">

                <div class="info-header" style="height: 45px">

                    <!-- <img src="../source/img/logo.jpg" alt=""> -->

                </div>

                <p>

                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, voluptatum enim excepturi dolores, quam fuga architecto quae quis laboriosam provident aliquid corporis repellendus quisquam cupiditate. Veritatis nobis necessitatibus vel exercitationem.

                </p>

                <a href="#" class="link">

                    <span class="icon"><i class="fas fa-globe-asia"></i></span>

                    <span class="hover hover-1">http://www.loremipsum.com</span>

                </a>

                </div>

                <div class="footer-right">

                <div class="footer-box-page">

                    <span class="box-title">Akun <span>Saya</span></span>

                    <ul>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Personal Info</span> 

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Order Saya</span> 

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Keranjang Belanja</span> 

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Wishlist</span> 

                        </a>

                    </li>

                    </ul>

                </div>

                <div class="footer-box-page">

                    <span class="box-title">Permat<span>apd</span> </span>

                    <ul>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Tentang Kami</span> 

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Jasa Kami</span> 

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Benefit</span> 

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Layanan</span>

                        </a>

                    </li>

                    <li>

                        <a href="#">

                        <span class="icon"><i class="fas fa-angle-right"></i></span>

                        <span class="hover hover-1">Kontak Kami</span> 

                        </a>

                    </li>

                    </ul>

                </div>

                </div>

            </div>
            
            </section>

            <section class="signature">

            <div class="left">

                brought to you by :

                <img src="../source/img/pgn_mas.png" alt="">

            </div>

            <div class="center">

                operated by :

                <img src="../source/img/kpusahatama.png" alt="">

            </div>

            <div class="right">

                E-Commerce Platform by :

                <img src="../source/img/dekodr.png" alt="">

            </div>

        </section>
        
        {script}
        <script type="text/javascript" src="../source/js/vendors/jquery-3.3.1.js">
        </script>
        <script type="text/javascript" src="../source/js/vendors/jquery-ui.js">
        </script>
        <script type="text/javascript" src="../source/js/app.js"></script>
        

    </body>
</html>