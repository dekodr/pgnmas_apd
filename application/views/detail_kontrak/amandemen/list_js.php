<script type="text/javascript">
<?php $admin = $this->session->userdata('admin'); ?>
$(function(){
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var __currency;
	
	var table = $('#tableAmandemen').tableGenerator({
		url: '<?php echo site_url('detail_kontrak/amandemen/getData/'.$id); ?>',
		data: dataPost,
		
		headers: [
			{
				"key"	: "no",
				"value"	: "Amendemen"
			},{
				"key"	: "date",
				"value"	: "Tanggal Amendemen"
			},{
				"key"	: "idr_value",
				"value"	: "Nilai Amendemen"
			},{
				"key"	: "contract_end",
				"value"	: "Tanggal Berakhir Pekerjaan"
			},{
				"key"	: "work_end",
				"value"	: "Tanggal Berakhir Garansi / Pemeliharaan"
			},{
				"key"	: "amandemen_file",
				"value"	: "Lampiran Amendemen"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				console.log(data);
				return '<p>Amandemen ke-'+data[0].value + '</p><p>No : ' + data[1].value +' </p><p>Perihal : ' + data[2].value +' </p>';
			},
			target : [0]
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(data[3].value);
			},
			target : [1]
		},
		{
			renderCell: function(data, row, key, el){
				return 'Rp '+$.number(data[4].value,0, '.',',');
			},
			target : [2]
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(data[5].value);
				
			},
			target : [3]
		},{
			renderCell: function(data, row, key, el){
				return defaultDate(data[6].value);
				
			},
			target : [4]
		},{
			renderCell: function(data, row, key, el){
				
				return '<a href="'+base_url+'assets/lampiran/'+data[8].key+'/'+data[8].value+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;</span></a>';
			},
			target : [5]
		},{
			renderCell: function(data, row, key, el){
				<?php if($admin['id_role']==3){ ?> 
				html =editButton(site_url+"detail_kontrak/amandemen/edit/"+data[9].value, data[9].value);
				html +=deleteButton(site_url+"detail_kontrak/amandemen/remove/"+data[9].value, data[9].value);
				<?php } ?>
				
				return html;
			},
			target : [6]
		}],
		
		additionFeature: function(el){
			<?php if($admin['id_role']==3){ ?> 
			el.append(insertButton(site_url+"detail_kontrak/amandemen/insert/<?php echo $id?>"));
			<?php } ?>
		},
		finish: function(){
			var edit = $('.buttonEdit').modal({
				header: 'Ubah Data',
				render : function(el, data){
					_self = edit;

					data.onSuccess = function(){
						$(edit).data('modal').close();
						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = false;
					
					$(el).form(data).data('form');

				}
			});

			var del = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){

						$(del).data('modal').close();

						table.data('plugin_tableGenerator').fetchData();
						
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});
		}
	});
	var add = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(){
				
				$(add).data('modal').close();
				table.data('plugin_tableGenerator').fetchData();
			}
			data.url = site_url+"detail_kontrak/amandemen/save/<?php echo $id?>";
			$(el).form(data);
		}
	});
});


</script>