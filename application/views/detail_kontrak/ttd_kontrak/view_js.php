<script type="text/javascript">
<?php $admin = $this->session->userdata('admin');?>
$(function(){
<?php if($dataBerkas['contract_type'] !=5 && $dataBerkas['contract_type'] !=2){ ?>
	$.ajax({
		url : '<?php echo site_url('detail_kontrak/ttd_kontrak/getSingleData/'.$id)?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			$('.formKontrak').form(xhr);
		}


	});

	$('#tabsKontrak').tabs();
	$('.tabsKontrak').tabs();
	
	var type = <?php echo $dataBerkas['contract_type'];?>;
	var isTermin = 0;
	var isBarang = 0;
	$('#listTab > li').hide();
	$('#tabsKontrak > div').hide();
	switch(type){
		case 1:
			isTermin = 1;
			break;
		case 2:
			isBarang = 1;
			break;
		case 3:
		default:
			isTermin = 1;
			isBarang = 1;
			break;
	}

	if(isTermin){
		$('a[href="#terminKontrak"]').parent().show();
		$('#terminKontrak').show();
	}
	if(isBarang){
		$('a[href="#barangKontrak"]').parent().show();
		if(isTermin){
			$('#barangKontrak').hide();
		}else{
			$('#barangKontrak').show();
		}
		
	}
	var tambahKontrak = $('.buttonTambahKontrak').modal({
		header: 'Tambah Kontrak',
		render : function(el, data){
			data.onSuccess = function(data){
				tambahKontrak.data('modal').close();
				window.location.href = '<?php echo site_url('kontrak/view/'.$id.'#kontrak'); ?>';
			}
			$(el).form(data);
		}
	});

	var ubah = $('.btn-ubah').modal({
		header: 'Ubah Data',
		render : function(el, data){
			el.html('<div class="blockWrapper"><div class="form"></div><div>');
			data.onSuccess = function(xhr){
				var current_index = $(".tab").tabs("option","active");
				$('#tabs').tabs('load',1);
				$(ubah).data('modal').close();
				window.location.href= '<?php echo site_url('kontrak/view/'.$id.'#kontrak'); ?>';
			};
			data.isReset = true;
			
			$('.form', el).form(data).data('form');

		}
	});


	var table1 = $('#tableTerminKontrak').tableGenerator({
		url: '<?php echo site_url('detail_kontrak/ttd_kontrak/termin/'.$id.'/'.$dataKontrak[0]['id']); ?>',
		data: {},
		
		headers: [
			{
				"key"	: "termin",
				"value"	: "Termin ke - "
			},{
				"key"	: "percen",
				"value"	: "Besaran (%)"
			},{
				"key"	: "value",
				"value"	: "Besaran (Rp.)"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				
				var html = '';
				
				if(!is_empty(data[2].value)){
					html += rupiah(data[2].value);
				}
				return html;
			},
			target : [2]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php if($admin['id_role']==3){ ?> 
				html +=editButton(site_url+"detail_kontrak/ttd_kontrak/editTermin/"+data[3].value, data[3].value);
				html +=deleteButton(site_url+"detail_kontrak/ttd_kontrak/deleteTermin/"+data[3].value, data[3].value);
				<?php } ?>
				return html;
			},
			target : [3]
		}],
		additionFeature: function(el){
				<?php if($admin['id_role']==3){ ?> 
				el.append(insertButton(site_url+"detail_kontrak/ttd_kontrak/insertTermin/<?php echo $id?>"));	
				<?php } ?>

		},
		finish: function(){
			var edit1 = $('.buttonEdit').modal({
		        render : function(el, data){
					data.onSuccess = function(){
						$(edit1).data('modal').close();
						table1.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = false;

					$(el).form(data).data('form');
		        }
     	 	});
			var del1 = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del1).data('modal').close();
						table1.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			
		}
	});
	var add1 = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(data){
				add1.data('modal').close();
				table1.data('plugin_tableGenerator').fetchData();
				
			}
			$(el).form(data);
		}
	});

	var table2 = $('#tableBarangKontrak').tableGenerator({
		url: '<?php echo site_url('detail_kontrak/barang/getBarang/'.$id); ?>',
		data: {},
		
		headers: [
			{
				"key"	: "nama_barang",
				"value"	: "Nama Barang / Jasa"
			},{
				"key"	: "quantity",
				"value"	: "Jumlah Barang / Satuan"
			},{
				"key"	: "contract_price",
				"value"	: "Harga satuan"
			},{
				"key"	: "action",
				"value"	: "Action",
				"sort"	: false
			}
		],
		columnDefs : [{
			renderCell: function(data, row, key, el){
				
				
				return data[1].value +' / ' + data[4].value;
			},
			target : [1]

		},{
			renderCell: function(data, row, key, el){
				
				var html = '';
				
				if(!is_empty(data[2].value)){
					html += rupiah(data[2].value);
				}
				return html;
			},
			target : [2]

		},{
			renderCell: function(data, row, key, el){
				var html = '';
				<?php if($admin['id_role']==3){ ?> 
				html =editButton(site_url+"detail_kontrak/barang/edit/"+data[5].value, data[5].value);
				html +=deleteButton(site_url+"detail_kontrak/barang/remove/"+data[5].value, data[5].value);
				<?php } ?>
				
				return html;
			},
			target : [3]
		}],
		additionFeature: function(el){
			<?php if($admin['id_role']==3){ ?> 
				el.append(insertButton(site_url+"detail_kontrak/barang/insert/<?php echo $id?>"));	
				<?php } ?>
		},
		finish: function(){
			var edit2 = $('.buttonEdit').modal({
		        render : function(el, data){
					data.onSuccess = function(){
						$(edit2).data('modal').close();
						table2.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = false;

					$(el).form(data).data('form');
		        }
     	 	});
			var del2 = $('.buttonDelete').modal({
				header: 'Hapus Data',
				render : function(el, data){
					el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
					data.onSuccess = function(){
						$(del2).data('modal').close();
						table2.data('plugin_tableGenerator').fetchData();
					};
					data.isReset = true;
					$('.form', el).form(data).data('form');
				}
			});

			
		}
	});
	var add2 = $('.buttonAdd').modal({
		render : function(el, data){
			data.onSuccess = function(data){
				add2.data('modal').close();
				table2.data('plugin_tableGenerator').fetchData();

			}
			$(el).form(data);
		}
	});


<?php }else{ 
	foreach ($dataKontrak as $key => $value) { 	
	?>
		$.ajax({
			url : '<?php echo site_url('detail_kontrak/ttd_kontrak/getSingleData/'.$id.'/'.$value['id'].'/'.$value['type'])?>',
			method: 'POST',
			async : false,
			dataType : 'json',
			success: function(xhr){
				$('#formKontrak<?php echo $value['id']?>').form(xhr);
			}
		});



		$('#tabsKontrak<?php echo $value['id']?>').tabs();
		$('.tabsKontrak').tabs();
		
		var type = <?php echo $dataBerkas['contract_type'];?>;
		var isTermin = 0;
		var isBarang = 0;
		$('#listTab > li').hide();
		$('#tabsKontrak > div').hide();
		switch(type){
			case 1:
				isTermin = 1;
				break;
			case 2:
				isBarang = 1;
				break;
			case 3:
			default:
				isTermin = 1;
				isBarang = 1;
				break;
		}

		if(isTermin){
			$('a[href="#terminKontrak"]').parent().show();
			$('#terminKontrak').show();
		}
		if(isBarang){
			$('a[href="#barangKontrak"]').parent().show();
			if(isTermin){
				$('#barangKontrak').hide();
			}else{
				$('#barangKontrak').show();
			}
			
		}
		var tambahKontrak = $('.buttonTambahKontrak').modal({
			header: 'Tambah Kontrak',
			render : function(el, data){
				data.onSuccess = function(data){
					tambahKontrak.data('modal').close();
				}
				$(el).form(data);
			}
		});

		var ubah = $('.btn-ubah').modal({
			header: 'Ubah Data',
			render : function(el, data){
				el.html('<div class="blockWrapper"><div class="form"></div><div>');
				data.onSuccess = function(xhr){
					var current_index = $(".tab").tabs("option","active");
					$('#tabs').tabs('load',1);
					$(ubah).data('modal').close();
					// location.reload = '<?php echo site_url('kontrak/view/'.$id.'#kontrak'); ?>';
				};
				data.isReset = true;
				
				$('.form', el).form(data).data('form');

			}
		});


		var table1 = $('#tableTerminKontrak').tableGenerator({
			url: '<?php echo site_url('detail_kontrak/ttd_kontrak/termin/'.$id.'/'.$dataKontrak[0]['id']); ?>',
			data: {},
			
			headers: [
				{
					"key"	: "termin",
					"value"	: "Termin ke - "
				},{
					"key"	: "percen",
					"value"	: "Besaran (%)"
				},{
					"key"	: "value",
					"value"	: "Besaran (Rp.)"
				},{
					"key"	: "action",
					"value"	: "Action",
					"sort"	: false
				}
			],
			columnDefs : [{
				renderCell: function(data, row, key, el){
					
					var html = '';
					
					if(!is_empty(data[2].value)){
						html += rupiah(data[2].value);
					}
					return html;
				},
				target : [2]

			},{
				renderCell: function(data, row, key, el){
					var html = '';
					<?php if($admin['id_role']==3){ ?> 
					html +=editButton(site_url+"detail_kontrak/ttd_kontrak/editTermin/"+data[3].value, data[3].value);
					html +=deleteButton(site_url+"detail_kontrak/ttd_kontrak/deleteTermin/"+data[3].value, data[3].value);
					<?php } ?>
					return html;
				},
				target : [3]
			}],
			additionFeature: function(el){
					<?php if($admin['id_role']==3){ ?> 
					el.append(insertButton(site_url+"detail_kontrak/ttd_kontrak/insertTermin/<?php echo $id?>"));	
					<?php } ?>

			},
			finish: function(){
				var edit1 = $('.buttonEdit').modal({
			        render : function(el, data){
						data.onSuccess = function(){
							$(edit1).data('modal').close();
							table1.data('plugin_tableGenerator').fetchData();
						};
						data.isReset = false;

						$(el).form(data).data('form');
			        }
	     	 	});
				var del1 = $('.buttonDelete').modal({
					header: 'Hapus Data',
					render : function(el, data){
						el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
						data.onSuccess = function(){
							$(del1).data('modal').close();
							table1.data('plugin_tableGenerator').fetchData();
						};
						data.isReset = true;
						$('.form', el).form(data).data('form');
					}
				});

				
			}
		});
		var add1 = $('.buttonAdd').modal({
			render : function(el, data){
				data.onSuccess = function(data){
					add1.data('modal').close();
					table1.data('plugin_tableGenerator').fetchData();
					
				}
				$(el).form(data);
			}
		});

		var table2 = $('#tableBarangKontrak').tableGenerator({
			url: '<?php echo site_url('detail_kontrak/barang/getBarang/'.$id); ?>',
			data: {},
			
			headers: [
				{
					"key"	: "nama_barang",
					"value"	: "Nama Barang / Jasa"
				},{
					"key"	: "quantity",
					"value"	: "Jumlah Barang / Satuan"
				},{
					"key"	: "contract_price",
					"value"	: "Harga satuan"
				},{
					"key"	: "action",
					"value"	: "Action",
					"sort"	: false
				}
			],
			columnDefs : [{
				renderCell: function(data, row, key, el){
					
					
					return data[1].value +' / ' + data[4].value;
				},
				target : [1]

			},{
				renderCell: function(data, row, key, el){
					
					var html = '';
					
					if(!is_empty(data[2].value)){
						html += rupiah(data[2].value);
					}
					return html;
				},
				target : [2]

			},{
				renderCell: function(data, row, key, el){
					var html = '';
					<?php if($admin['id_role']==3){ ?> 
					html =editButton(site_url+"detail_kontrak/barang/edit/"+data[5].value, data[5].value);
					html +=deleteButton(site_url+"detail_kontrak/barang/remove/"+data[5].value, data[5].value);
					<?php } ?>
					
					return html;
				},
				target : [3]
			}],
			additionFeature: function(el){
				<?php if($admin['id_role']==3){ ?> 
					el.append(insertButton(site_url+"detail_kontrak/barang/insert/<?php echo $id?>"));	
					<?php } ?>
			},
			finish: function(){
				var edit2 = $('.buttonEdit').modal({
			        render : function(el, data){
						data.onSuccess = function(){
							$(edit2).data('modal').close();
							table2.data('plugin_tableGenerator').fetchData();
						};
						data.isReset = false;

						$(el).form(data).data('form');
			        }
	     	 	});
				var del2 = $('.buttonDelete').modal({
					header: 'Hapus Data',
					render : function(el, data){
						el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
						data.onSuccess = function(){
							$(del2).data('modal').close();
							table2.data('plugin_tableGenerator').fetchData();
						};
						data.isReset = true;
						$('.form', el).form(data).data('form');
					}
				});

				
			}
		});
		var add2 = $('.buttonAdd').modal({
			render : function(el, data){
				data.onSuccess = function(data){
					add2.data('modal').close();
					table2.data('plugin_tableGenerator').fetchData();

				}
				$(el).form(data);
			}
		});

	<?php
	}
} ?>
	
});


</script>