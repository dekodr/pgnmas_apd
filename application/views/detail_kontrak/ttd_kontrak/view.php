<?php if($dataBerkas['contract_type'] !=5&& $dataBerkas['contract_type'] !=2){ ?>
<div class="mg-lg-7 ">
	<div class="block ">
		<div class="formKontrak">
		</div>
		<div class="form-group btn-group">
			<?php $admin= $this->session->userdata('admin'); ?>
			<?php if($admin['id_role']==3){ ?> 
			<a href="<?php echo site_url('detail_kontrak/ttd_kontrak/edit/'.$id)?>" class="btn btn-warning btn-ubah"><i class="fa fa-gears"></i>&nbsp;Ubah</a>
			<?php } ?>
		</div>
		

	</div>
</div>
<div class="mg-lg-12 ">
	<div id="tabsKontrak">
		<ul class="nav-tabs" id="listTab">
			<li ><a href="#terminKontrak">Termin Kontrak</a></li>
			<li ><a href="#barangKontrak">Barang / Jasa</a></li>
		</ul>
		<div id="terminKontrak">
			<div id="tableTerminKontrak">
			</div>
		</div>
		<div id="barangKontrak">
			<div id="tableBarangKontrak">
			</div>
		</div>
	</div>

</div>
<?php } else { ?>
<div class="mg-lg-12 row">
	<a class="btn btn-primary buttonTambahKontrak row"  href="<?php echo site_url('detail_kontrak/ttd_kontrak/insert/'.$id)?>"><i class="fa fa-file"></i>&nbsp; Tambah Kontrak</a>
	<?php if($dataBerkas['contract_type']==5 || $dataBerkas['contract_type'] ==2){ ?>
	<a class="btn btn-primary buttonTambahKontrak row"  href="<?php echo site_url('detail_kontrak/ttd_kontrak/insert/'.$id.'/2')?>"><i class="fa fa-file"></i>&nbsp; Tambah SPK</a>
	<?php } ?>
	<div class="tabsKontrak">
		<ul class="nav-tabs">
			<?php foreach ($dataKontrak as $key => $value) { ?>
				<li style="display: list-item;"><a href="#tabKontrak<?php echo $value['id'];?>"><?php echo $value['no_contract']?></a></li>
			<?php } ?>
			
		</ul>
		<?php foreach ($dataKontrak as $key => $value) {
				?>
		<div id="tabKontrak<?php echo $value['id'];?>">
			<div class="mg-lg-12">
				<div class="mg-lg-7">
					<div class="block ">
						<div id="formKontrak<?php echo $value['id'];?>" data-id="<?php echo $value['id'];?>">
						</div>
						<div class="form-group btn-group">
							<?php $admin= $this->session->userdata('admin'); ?>
							<?php if($admin['id_role']==3){ ?> 
							<a href="<?php echo site_url('detail_kontrak/ttd_kontrak/edit/'.$id.'/'.$value['type'])?>" class="btn btn-warning btn-ubah"><i class="fa fa-gears"></i>&nbsp;Ubah</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php if($value['type']!=2){ ?>
				<div class="mg-lg-12 ">
					<div class="tabsKontrak">
						<ul class="nav-tabs" id="listTab">
							<li ><a href="#terminKontrak">Termin Kontrak</a></li>
							<li ><a href="#barangKontrak">Barang / Jasa</a></li>
						</ul>
						<div id="terminKontrak">
							<div id="tableTerminKontrak">
							</div>
						</div>
						<div id="barangKontrak">
							<div id="tableBarangKontrak">
							</div>
						</div>
					</div>

				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
	
</div>
<?php } ?>