<script type="text/javascript">
<?php $admin = $this->session->userdata('admin'); ?>
	dataPost = {
		order: 'id',
		sort: 'desc'
	};
	var __currency;
	$('#tableBap').ready(function(){
		var table = $('#tableBap').tableGenerator({
			url: '<?php echo site_url('detail_kontrak/bap/getData/'.$id); ?>',
			data: dataPost,
			
			headers: [
				{
					"key"	: "no",
					"value"	: "Nomor"
				},{
					"key"	: "date",
					"value"	: "Tanggal"
				},{
					"key"	: "value",
					"value"	: "Nilai"
				},{
					"key"	: "attachment_file",
					"value"	: "Lampiran"
				},{
					"key"	: "action",
					"value"	: "Action",
					"sort"	: false
				}
			],
			columnDefs : [{
				renderCell: function(data, row, key, el){
					return defaultDate(row);
				},
				target : [1]
			},
			{
				renderCell: function(data, row, key, el){
					console.log(data);
					return 'Rp '+$.number(row,0, '.',',');
				},
				target : [2]
			},{
				renderCell: function(data, row, key, el){
					
					return '<a href="'+base_url+'assets/lampiran/'+data[4].key+'/'+data[4].value+'" target="_blank"><span><i class="fa fa-download"></i>&nbsp;</span></a>';
				},
				target : [3]
			},{
				renderCell: function(data, row, key, el){
					<?php if($admin['id_role']==3){ ?> 
					html =editButton(site_url+"detail_kontrak/bap/edit/<?php echo $id; ?>/"+data[5].value, data[5].value);
					html +=deleteButton(site_url+"detail_kontrak/bap/remove/"+data[5].value, data[5].value);
					<?php } ?>
					return html;
				},
				target : [4]
			}],
			
			additionFeature: function(el){
				<?php if($admin['id_role']==3){ ?> 
				el.append(insertButton(site_url+"detail_kontrak/bap/insert/<?php echo $id?>"));
				<?php } ?>
			},
			finish: function(){
				var edit = $('.buttonEdit').modal({
					header: 'Ubah Data',
					render : function(el, data){
						_self = edit;

						data.onSuccess = function(){
							$(edit).data('modal').close();
							table.data('plugin_tableGenerator').fetchData();
							
						};
						data.isReset = false;
						
						$(el).form(data).data('form');

					}
				});

				var del = $('.buttonDelete').modal({
					header: 'Hapus Data',
					render : function(el, data){
						
						el.html('<div class="blockWrapper"><span>Apakah anda yakin ingin menghapus data?<span><div class="form"></div><div>');
						data.onSuccess = function(){

							$(del).data('modal').close();

							table.data('plugin_tableGenerator').fetchData();
							
						};
						data.isReset = true;
						$('.form', el).form(data).data('form');
					}
				});
			}
		});
		var add = $('.buttonAdd').modal({
			render : function(el, data){

				data.onSuccess = function(){
					
					$(add).data('modal').close();
					table.data('plugin_tableGenerator').fetchData();
				}
				data.url = site_url+"detail_kontrak/bap/save/<?php echo $id?>";

				$(el).form(data);
				var price = 0;

				var id_termin = $('.form-control [name="id_termin"]');
				console.log(id_termin.val());
				id_termin.on('change ready', function(){
					price = data.form[0].detail[parseInt(id_termin.val())];
				})
				console.log(price);
				
			}
		});
	})


</script>