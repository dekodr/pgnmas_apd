
<div class="mg-lg-12">
	<div class="block">

		<!-- <h4>Grafik Progress</h4>

		<div class="graphBar clearfix">
			<div class="graphBarGroup clearfix barPengadaan" style="width: 100%;">
				<table width="100%">
					<tr class="graphWrap">
					<?php if(isset($pengadaan)){ ?>

						<?php foreach($pengadaan as $key => $row){ ?>
							<td class="graph" title="<?php echo $row['value'];?> (Tanggal : <?php echo default_date($row['date'])?>) " style="width:<?php echo $row['percent'];?>%;background: <?php echo $row['color'];?>"></td>
						<?php } ?>
					<?php } ?>
					<tr>
				</table>
			</div>
			<ul>
			<?php if(isset($pengadaan)){ ?>
				<?php foreach($pengadaan as $_key => $_row){ ?>
				<li style="margin-bottom: 5px; padding-left: 10px; border-left: 15px solid <?php echo $_row['color'];?>;"><?php echo $_row['value'];?></li>
				<?php } ?>
			<?php } ?>
			</ul>
			<p class="notifReg"><i>*Arahkan Mouse untuk melihat keterangan pada Grafik Progress</i></p>
		</div> -->
		
		<form action="<?php echo site_url('detail_kontrak/progress_pengadaan/simpan/'.$id)?>" method="POST" id="formprogress_pengadaan">
			<div id="tableprogress_pengadaan">
			</div>	
			<?php if($dataBerkas['step'] == 0){ ?>
			<div class="form-group btn-group">
				<input type="hidden" name="id" value="<?php echo $id;?>">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			</div>	
			<?php } ?>
			
		</form>
		<form action="<?php echo site_url('detail_kontrak/progress_pengadaan/lanjut/'.$id)?>" method="POST" id="formlanjut_pengadaan">
			<?php if($dataBerkas['step'] == 1){ ?>
			<div class="form-group btn-group">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Simpan &amp; Lanjut</button>
			</div>	
			<?php } ?>
		</form>
	</div>
</div>