<script type="text/javascript">
<?php $user = $this->session->userdata('user'); ?>
$(function(){
	$.ajax({
		url : '<?php echo site_url('administrasi/edit/'.$user['id_user'])?>',
		method: 'POST',
		async : false,
		dataType : 'json',
		success: function(xhr){
			// console.log(xhr);
			xhr.onSuccess = function(data){
				window.location = '<?php echo site_url('administrasi');?>';

			}
			xhr.successMessage = 'Berhasil Mendaftarkan Penyedia Barang / Jasa Baru';

			$('.form').form(xhr);
		}


	});
});


</script>