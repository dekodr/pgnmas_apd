<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public $admin;

	public function __construct(){
		parent::__construct();
		
		$this->admin = $this->session->userdata('admin');
		$this->load->model('Main_model', 'mm');

	}

	public function index($id=null){
		$admin = $this->session->userdata('admin');
		$data 						= '';
		
		$this->header = 'Selamat Datang '.$this->admin['name'];
		$this->content = $this->parser->parse('dashboard/dashboard_admin', $data, TRUE);
		$this->script = $this->load->view('dashboard/dashboard_js', $data, TRUE);

		parent::index();
	}
	
	public function search_data(){
		echo json_encode($this->mm->search_data());
	}

	public function delete($id)
	{
		if ($this->mm->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function delete_notif($id)
	{
		$this->formDelete['url'] = site_url('dashboard/delete/' . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	
	public function view($view) {
		// $this->load->view('template/catalog/build/'.$view);
		
		// $this->header = 'Selamat Datang '.$this->admin['name'];
		$this->content = $this->parser->parse('template/catalog/build/'.$view, $data, TRUE);
		$this->script = $this->load->view('dashboard/dashboard_js', $data, TRUE);

		parent::index();
	}
	
}
