<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends MY_Controller {

	public $admin;

	public function __construct(){
		parent::__construct();
		
		$this->admin = $this->session->userdata('admin');
		$this->load->model('Main_model', 'mm');
		$this->load->model('Main_model', 'mm');

	}

	public function index($id=null){
		$admin = $this->session->userdata('admin');
		$data 						= '';

		$this->header = 'Selamat Datang '.$this->admin['name'];
		$this->headertitle = 'Katalog <span>Produk</span>';
		$this->content = $this->parser->parse('catalog/index', $data, TRUE);
		// $this->script = $this->load->view('catalog/index_js', $data, TRUE);
		

		$data = array(
			'user' => ($user) ? $user['name'] : $admin['name'],
			'header' => $this->header,
			'headertitle' => $this->headertitle,
			'content' => $this->content,
			'script' => $this->script
		);
		$this->parser->parse('template/base_catalog', $data);
	}

	
}
