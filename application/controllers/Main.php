<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();

		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('Main_model', 'mm');

	}
	public function index(){

		/*
		| -------------------------------------------------------------------
		|  Authentication users login
		| -------------------------------------------------------------------
		| These function prevented visitor to open restricted page.
		|
		| Prototype:
		|
		|	if($this->session->userdata('user')){
		|		redirect(site_url(''));
		|	}else{
		|		$this->login();
		|	}
		|
		*/

		if ($this->session->userdata('admin')){
			redirect(base_url('dashboard'));
		}else{

			$this->load->view('login');
		}
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url());
	}

	public function custom_query(){
		$this->mm->custom_query();
	}
	
	public function login(){

		if($this->input->post('username')&&$this->input->post('password')){
			// print_r($this->input->post());die;
			$is_logged = $this->mm->cek_login();

			if($is_logged == "ok"){
				/*if everything's cool*/			
				// if($this->session->userdata('admin')){
					$data = $this->session->userdata('admin');
					redirect(base_url('dashboard'));

				// }
				// print_r($this->session->userdata());die;
			}else if($is_logged=="oldpw"){
				/*if username OK but password was changed*/
				// redirect(base_url());
			}else{
				/*if there's something wrong*/
				$this->session->set_flashdata('error_msg','<p class="error_msg animated bounceInDown"><b>Data tidak dikenal.</b> Silahkan login kembali!</p>');
				redirect(base_url());				
			}
		}else{
			/*if there's empty textbox*/
			$this->session->set_flashdata('error_msg','<p class="error_msg animated bounceInDown">Isi form dengan benar!</p>');
			redirect(base_url());
		}
	}

	public function redirect(){
		$this->load->view('main/redirect');
	}
	public function view($view) {
		$this->load->view('template/catalog/build/'.$view);
	}
	
}