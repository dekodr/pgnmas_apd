<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	public $id_client;

	public $_sideMenu;

	public $breadcrumb;

	public $header;

	public $content;

	public $script;

	public $form;

	public $activeMenu;

	public $successMessage = '<div class="alert alert-success temp">Sukses</div>';

	public $isClientMenu;

	function __construct(){
		parent::__construct();
		$this->_sideMenu = array();
		$this->load->library('breadcrumb', array());
		$this->form_validation->set_error_delimiters('', '');
		if ($this->uri->segment(1) != '') {
			if (!$this->session->userdata('admin')) {
				redirect(site_url());
			}
		}
	}

	function index($id = null){

		/*
		| -------------------------------------------------------------------
		|  Basic Structure of pages
		| -------------------------------------------------------------------
		*/
		$this->isAdmin();
		$this->breadcrumb = $this->breadcrumb->generate();
		$this->load->library('sideMenu', $this->_sideMenu);
		$user = $this->session->userdata('user');
		$admin = $this->session->userdata('admin');
		$data = array(
			'user' => ($user) ? $user['name'] : $admin['name'],
			'sideMenu' => $this->sidemenu->generate($this->activeMenu) ,
			'breadcrumb' => $this->breadcrumb,
			'header' => $this->header,
			'content' => $this->content,
			'script' => $this->script
		);
		$this->parser->parse('template/base', $data);
	}
	
	function catalog($id = null){

		/*
		| -------------------------------------------------------------------
		|  Basic Structure of pages
		| -------------------------------------------------------------------
		*/
		$this->load->library('sideMenu', $this->_sideMenu);
		$admin = $this->session->userdata('admin');
		$data = array(
			'user' => ($user) ? $user['name'] : $admin['name'],
			'header' => $this->header,
			'headertitle' => $this->headertitle,
			'content' => $this->content,
			'script' => $this->script
		);
		$this->parser->parse('template/base_catalog', $data);
	}

	function formFilter(){
		$return['button'] = array(
			array(
				'type' => 'button',
				'label' => 'Filter',
				'class' => 'btn-filter'
			) ,
			array(
				'type' => 'reset',
				'label' => 'Reset'
			)
		);
		$return['form'] = $this->form['filter'];
		echo json_encode($return);
	}

	function isAdmin(){
		if ($this->session->userdata('admin')) {
			$this->_sideMenu = array(
				array(
					'group' => 'dashboard',
					'title' => 'Dashboard',
					'icon' => 'home',
					'url' => site_url() ,
					'role' => array(
						1,
						2,
						3
					)
				),
				array(
					'group' => 'master',
					'title' => 'Master',
					'icon' => 'database',
					'url' => '#' ,
					'role' => array(
						1,
						2,
						3
					),
					'list' => array(
						array(
							'url' => base_url('master/user') ,
							'title' => 'User',
							'role' => array(
								1,
								2,
								3,
								6
							),
						),
						array(
							'url' => base_url('master/barang') ,
							'title' => 'Barang',
							'role' => array(
								1,
								2,
								3,
								6
							),
						),
						array(
							'url' => base_url('pemaketan/division/'.$admin['id_division']),
							'title' => 'Perencanaan Pengadaan',
							'role' => array(
								4,
								5,
							)
						)
					)
				)
			);
		}
	}

	public	function validation($form = null){

		ob_start();

		$_r = false;
		if ($form == null) {
			$form = $this->form['form'];
			$this->form_validation->set_rules($this->form['form']);
		}

		if ($this->form_validation->run() == FALSE) {

			$return['status'] = 'error';
			foreach($form as $value) {
				if ($value['type'] == 'file') {
					$return['file'][$value['field']] = $this->session->userdata($value['field']);
				}

				if ($value['type'] == 'date_range') {
					$return['form'][$value['field'][0]] = form_error($value['field'][0] . '_start');
					$return['form'][$value['field'][1]] = form_error($value['field'][1] . '_start');
				}
				else {
					$return['form'][$value['field']] = form_error($value['field']);
				}
			}

			$_r = false;
		}
		else {
			$return['status'] = 'success';
			$_r = true;
		}

		echo json_encode($return);
		return $_r;
	}

	public function getData($id = null){

		$config['query'] = $this->getData;
		$return = $this->tablegenerator->initialize($config);
		echo json_encode($return);
	}

	public function insert()
	{
		$this->form['url'] = $this->insertUrl;
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Simpan',
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function save($data = null)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$save['entry_stamp'] = timestamp();
			if ($this->$modelAlias->insert($save)) {
				$this->session->set_flashdata('msg', $this->successMessage);
				$this->deleteTemp($save);
				return true;
			}
		}
	}

	public function edit($id = null)
	{
		$modelAlias = $this->modelAlias;
		$data = $this->$modelAlias->selectData($id);
		
		foreach($this->form['form'] as $key => $element) {
			$this->form['form'][$key]['value'] = $data[$element['field']];
			if($this->form['form'][$key]['type']=='date_range'){
				$_value = array();
				
				foreach ($this->form['form'][$key]['field'] as $keys => $values) {
					$_value[] = $data[$values];
					
				}
				$this->form['form'][$key]['value'] = $_value;
			}
		}


		$this->form['url'] = site_url($this->updateUrl . '/' . $id);
		$this->form['button'] = array(
			array(
				'type' => 'submit',
				'label' => 'Ubah'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->form);
	}

	public function update($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->validation()) {
			$save = $this->input->post();
			$lastData = $this->$modelAlias->selectData($id);
			if ($this->$modelAlias->update($id, $save)) {
				$this->session->set_userdata('alert', $this->form['successAlert']);
				$this->deleteTemp($save, $lastData);
			}
		}
	}
	 public function getSingleData($id){
        $user  = $this->session->userdata('user');
        $modelAlias = $this->modelAlias;
        $getData   = $this->$modelAlias->selectData($id);
      
        foreach($this->form['form'] as $key => $value){
            $this->form['form'][$key]['readonly'] = TRUE;
            $this->form['form'][$key]['value'] = $getData[$value['field']];
           
            if($value['type']=='date_range'){
                foreach($value['field'] as $keyField =>$rowField){
                    $this->form['form'][$key]['value'][] = $getData[$rowField];
                }
                
            }
            if($value['type']=='money'){
                    $this->form['form'][$key]['value'] = number_format($getData[$value['field']]);
                }
            if($value['type']=='money_asing'){
                $this->form['form'][$key]['value'][] = $getData[$value['field'][0]];
                $this->form['form'][$key]['value'][] = number_format($getData[$value['field'][1]]);
            }   
        }

        echo json_encode($this->form);
    }
	public function approveOvertimeUser($id)
	{
		$modelAlias = $this->modelAlias;
		$save = $this->input->post();
		$save['edit_stamp'] = timestamp();
		return $this->$modelAlias->update($id, $save);
	}

	public function delete($id)
	{
		$modelAlias = $this->modelAlias;
		if ($this->$modelAlias->delete($id)) {
			$return['status'] = 'success';
		}
		else {
			$return['status'] = 'error';
		}

		echo json_encode($return);
	}

	public function remove($id)
	{
		$this->formDelete['url'] = site_url($this->deleteUrl . $id);
		$this->formDelete['button'] = array(
			array(
				'type' => 'delete',
				'label' => 'Hapus'
			) ,
			array(
				'type' => 'cancel',
				'label' => 'Batal'
			)
		);
		echo json_encode($this->formDelete);
	}

	public function upload_lampiran()
	{
		
		foreach($_FILES as $key => $row) {
			if(is_array($row['name'])){
				foreach ($row['name'] as $keys => $values) {
					$file_name = $row['name'] = $key . '_' . name_generator($_FILES[$key]['name'][$keys]);
					 $_FILES['files']['name']= $file_name;
			        $_FILES['files']['type']= $_FILES[$key]['type'][$keys];
			        $_FILES['files']['tmp_name']= $_FILES[$key]['tmp_name'][$keys];
			         $_FILES['files']['error']= $_FILES[$key]['error'][$keys];
			         $_FILES['files']['size']= $_FILES[$key]['size'][$keys];
					
					$config['upload_path'] = './assets/lampiran/temp/';
					$config['allowed_types'] = $_POST['allowed_types'];
					$this->load->library('upload');
					$this->upload->initialize($config);

					if (!$this->upload->do_upload('files')) {
						$return['status'] = 'error';
						$return['message'] = $this->upload->display_errors('', '');
					}
					else {
						$return['status'] = 'success';
						$return['upload_path'] = base_url('assets/lampiran/temp/' . $file_name);
						$return['file_name'] = $file_name;
					}

					echo json_encode($return);
				}
				
			}else{
				$file_name = $_FILES[$key]['name'] = $key . '_' . name_generator($_FILES[$key]['name']);
				$config['upload_path'] = './assets/lampiran/temp/';
				$config['allowed_types'] = $_POST['allowed_types'];
				$this->load->library('upload');
				$this->upload->initialize($config);
				if (!$this->upload->do_upload($key)) {
					$return['status'] = 'error';
					$return['message'] = $this->upload->display_errors('', '');
				}
				else {
					$return['status'] = 'success';
					$return['upload_path'] = base_url('assets/lampiran/temp/' . $file_name);
					$return['file_name'] = $file_name;
				}

				echo json_encode($return);
			}
			
		}
	}

	public function do_upload($field, $db_name = '')
	{
		$file_name = $_FILES[$db_name]['name'] = $db_name . '_' . name_generator($_FILES[$db_name]['name']);
		$config['upload_path'] = './assets/lampiran/' . $db_name . '/';
		$config['allowed_types'] = 'pdf|jpeg|jpg|png|gif';
		$this->load->library('upload');
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($db_name)) {
			$_POST[$db_name] = $file_name;
			$this->form_validation->set_message('do_upload', $this->upload->display_errors('', ''));
			return false;
		}
		else {
			$this->session->set_userdata($db_name, $file_name);
			$_POST[$db_name] = $file_name;
			return true;
		}
	}

	public function deleteTemp($save, $lastData = null)
	{
		
		foreach($this->form['form'] as $key => $value) {
			if ($value['type'] == 'file') {
				if ($lastData != null && ($save[$value['field']] != $lastData[$value['field']])) {
					if ($lastData[$value['field']] != '') {
						unlink('./assets/lampiran/' . $value['field'] . '/' . $lastData[$value['field']]);
					}
				}

				if ($save[$value['field']] != '') {
					if (file_exists('./assets/lampiran/temp/' . $save[$value['field']])) {
						rename('./assets/lampiran/temp/' . $save[$value['field']], './assets/lampiran/' . $value['field'] . '/' . $save[$value['field']]);
					}
				}
			}
		}
	}
}
